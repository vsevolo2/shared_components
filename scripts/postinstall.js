const path = require('path');
const fs = require('fs');

console.log("==> remove '@charset \"UTF-8\";' from tabler.min.css");

const file = path.resolve(__dirname, '../node_modules/@tabler/core/dist/css/tabler.min.css');
let data = fs.readFileSync(file, {encoding:'utf8'});

data = data.replace('@charset "UTF-8";', '');

fs.writeFileSync(file, data, {encoding:'utf8'});
