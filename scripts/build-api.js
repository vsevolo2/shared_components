const path = require("path");
const rimraf = require("rimraf");
const fs = require("fs");
const { execSync } = require("child_process");
const axios = require("axios");
require("dotenv").config();

const OA_URL =
  process.env.API_DOCS_URL ?? "https://api.gkh911.ru/swagger/person/docs.yaml";
const API_DIR = path.resolve(__dirname, "../api");
const BUILD_DIR = path.resolve(__dirname, "../api-build");

const changeFileContent = (fileName, cb) => {
  let body = fs.readFileSync(fileName, { encoding: "utf8" });
  body = cb(body);
  fs.writeFileSync(fileName, body, { encoding: "utf8" });
};

const main = async function () {
  // удаляем папки (если есть) api и api-build
  console.log("==> rm ./api ./api-build");
  fs.existsSync(API_DIR) && rimraf.sync(API_DIR);
  fs.existsSync(BUILD_DIR) && rimraf.sync(BUILD_DIR);

  // создаем папку api-build и переходим в неё
  console.log("==> mkdir ./api-build; cd ./api-build");
  fs.mkdirSync(BUILD_DIR);
  process.chdir(BUILD_DIR);

  // вытягиваем yaml файл с удаленного сервера
  console.log("==> wget " + OA_URL);
  const response = await axios.get(OA_URL);
  fs.writeFileSync("docs.yaml", response.data, { encoding: "utf8" });

  // генерим доку
  console.log("==> swagger-codegen generate");
  execSync(
    "swagger-codegen generate " +
      "-l typescript-axios " +
      "--additional-properties modelPropertyNaming=original " +
      // "--model-name-prefix Api " +
      // "--model-name-suffix Api " +
      "-i docs.yaml",
    { encoding: "utf8" }
  );

  // подправляем tsconfig.json
  console.log("==> fix tsconfig.json");
  const config = {
    compilerOptions: {
      declaration: true,
      target: "esnext",
      module: "es6",
      noImplicitAny: true,
      outDir: "dist",
      rootDir: ".",
      lib: ["es6", "dom"],
      typeRoots: ["node_modules/@types"],
      moduleResolution: "node",
    },
    exclude: ["dist", "node_modules"],
  };
  fs.writeFileSync("tsconfig.json", JSON.stringify(config, null, 2), {
    encoding: "utf8",
  });

  // собираем api в папку dist
  console.log("==> build api");
  execSync("npm install; npm run build", { encoding: "utf8" });

  // двигаем ./api-build/dist на конечное место ./api
  fs.renameSync(BUILD_DIR + "/dist", API_DIR);

  // подчищаем за собой
  console.log("==> cleanup");
  process.chdir(API_DIR);
  rimraf.sync(BUILD_DIR);

  // а здесь можно что-то подправить в сгенерированном АПИ ...

  // так как мы всегда перезаписываем BASE_PATH своим значением, то и установим BASE_PATH = localhost
  // это не влияет на исполнение, но зато при сборках на разных дев машинах изменений гите из-за BASE_PATH не будет
  console.log("==> fix BASE_PATH");
  changeFileContent(API_DIR + "/base.js", function (body) {
    return body.replace(
      /^export const BASE_PATH =.+$/gm,
      'export const BASE_PATH = "http://localhost/api"'
    );
  });

  console.log("==> add property PersonalAccountAddress.short_address");
  changeFileContent(
    API_DIR + "/models/personal-account-address.d.ts",
    function (body) {
      const replaceBy =
        "    address: string;\n" +
        "    /**\n" +
        "     * [*] Краткий адрес (без города и текста КВ.)\n" +
        "     * @type {string}\n" +
        "     * @memberof PersonalAccountAddress\n" +
        "     */\n" +
        "     short_address: string;";
      return body.replace(/^\s+address: string;$/gm, replaceBy);
    }
  );
};

main().then(() => {});
