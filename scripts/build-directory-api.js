const path = require('path');
const rimraf = require('rimraf');
const fs = require('fs');
const { execSync } = require('child_process');
const axios = require('axios');


const DIRECTORY_URL = process.env.DIRECTORY_DOC_URL??'https://api.gkh911.ru/swagger/directory/docs.yaml';
const DIRECTORY_DIR = path.resolve(__dirname, '../directory');
const BUILD_DIRECTORY_DIR = path.resolve(__dirname, '../directory-build');

const main = async function ()
{
    // удаляем папки (если есть) directory и directory-build
    console.log("==> rm ./directory ./directory-build");
    fs.existsSync(DIRECTORY_DIR) && rimraf.sync(DIRECTORY_DIR)
    fs.existsSync(BUILD_DIRECTORY_DIR) && rimraf.sync(BUILD_DIRECTORY_DIR)

    // создаем папку directory-build и переходим в неё
    console.log("==> mkdir ./directory-build; cd ./directory-build");
    fs.mkdirSync(BUILD_DIRECTORY_DIR)
    process.chdir(BUILD_DIRECTORY_DIR);

    // вытягиваем yaml файл с удаленного сервера
    console.log("==> wget " + DIRECTORY_URL);
    const responseDirectory = await axios.get(DIRECTORY_URL);
    fs.writeFileSync('docs.yaml', responseDirectory.data, {encoding: 'utf8'});

    //directory
    process.chdir(BUILD_DIRECTORY_DIR);
    // генерим доку
    console.log("==> swagger-codegen generate");
    execSync('swagger-codegen generate ' +
             '-l typescript-axios ' +
             '--additional-properties modelPropertyNaming=original ' +
            '-i docs.yaml',
        {encoding: 'utf8'}
    );

    // подправляем tsconfig.json
    console.log("==> fix tsconfig.json");
    const config = {
        "compilerOptions": {
            "declaration": true,
            "target": "esnext",
            "module": "es6",
            "noImplicitAny": true,
            "outDir": "dist",
            "rootDir": ".",
            "lib": ["es6", "dom"],
            "typeRoots": ["node_modules/@types"],
            "moduleResolution": "node"
        },
        "exclude": ["dist", "node_modules"]
    }
    fs.writeFileSync('tsconfig.json', JSON.stringify(config, null, 2), {encoding: 'utf8'});
    // собираем directory в папку dist
    console.log("==> build directory");
    execSync('npm install; npm run build', {encoding: 'utf8'});

    // двигаем ./directory-build/dist на конечное место ./directory
    fs.renameSync(BUILD_DIRECTORY_DIR + '/dist', DIRECTORY_DIR)

    // подчищаем за собой
    console.log("==> cleanup");
    process.chdir(DIRECTORY_DIR);
    rimraf.sync(BUILD_DIRECTORY_DIR);

    // а здесь можно что-то подправить в сгенерированном АПИ ...
    console.log("==> fix BASE_PATH");

    // так как мы всегда перезаписываем BASE_PATH своим значением, то и установим BASE_PATH = localhost
    // это не влияет на исполнение, но зато при сборках на разных дев машинах изменений гите из-за BASE_PATH не будет
    let body = fs.readFileSync(DIRECTORY_DIR + "/base.js", {encoding: 'utf8'});
    body = body.replace(/^export const BASE_PATH =.+$/mg, 'export const BASE_PATH = "http://localhost/api"');
    fs.writeFileSync(DIRECTORY_DIR + "/base.js", body, {encoding: 'utf8'});
}

main().then(() => {})
