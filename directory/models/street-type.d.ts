/**
 * ORS Api (Directory)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface StreetType
 */
export interface StreetType {
    /**
     * Уникальный идентификатор
     * @type {number}
     * @memberof StreetType
     */
    id: number;
    /**
     * Название вида улицы
     * @type {string}
     * @memberof StreetType
     */
    name: string;
}
