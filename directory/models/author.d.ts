/**
 * ORS Api (Directory)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Author
 */
export interface Author {
    /**
     * Уникальный идентификатор
     * @type {number}
     * @memberof Author
     */
    id: number;
    /**
     * Название
     * @type {string}
     * @memberof Author
     */
    name: string;
}
