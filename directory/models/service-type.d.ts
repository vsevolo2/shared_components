/**
 * ORS Api (Directory)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ServiceType
 */
export interface ServiceType {
    /**
     * Уникальный идентификатор
     * @type {number}
     * @memberof ServiceType
     */
    id: number;
    /**
     * Название
     * @type {string}
     * @memberof ServiceType
     */
    name: string;
}
