import { Profile } from "~/store";

export default defineNuxtRouteMiddleware((from, to) => {
  const local = useLocal();
  const user = Profile();

  if ((from.name.includes('app-appeals') && user.appealBlocked())
    || (from.name === 'app-person-meters' && user.inputIpuBlocked()))
  {
    return navigateTo('/');
  }

  if (!local.get() && user.isNameRequired) {
    return navigateTo('/app/profile');
  }
});
