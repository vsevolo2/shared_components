import { UserDevice } from '~/store';
import { Profile } from "~/store";

export default defineNuxtRouteMiddleware((to, from) => {
  // переключение ЛС при переходе по ссылке
  const local = useLocal();
  const user = Profile();
  const curAccount = user.currentAccount();

  if (Object.keys(from.query).includes('account')){
    const queryAccount = user.accountById(from.query.account as string);
    if (queryAccount && (queryAccount !== curAccount)) {
      local.set(queryAccount.id);
      location.reload();
    }
  }

  if (process.client)
  {
    const token = useApiToken();
    const userDevice = UserDevice();

    if (!token.exists())
    {
      userDevice.unregister();
      return navigateTo('/app/auth/login');
    }

    const centrifugo = useCentrifugo();
    centrifugo.state == 'disconnected' && centrifugo.connect();
    useNuxtApp().$requestPermission && useNuxtApp().$requestPermission();
  }
});
