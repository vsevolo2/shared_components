/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AppealRequest
 */
export interface AppealRequest {
    /**
     * Уникальный идентификатор ЛС
     * @type {number}
     * @memberof AppealRequest
     */
    personal_account_id: number;
    /**
     * Номер телефона
     * @type {string}
     * @memberof AppealRequest
     */
    phone?: string;
    /**
     * Содержание заявки
     * @type {string}
     * @memberof AppealRequest
     */
    desc?: string;
    /**
     * Дата для визита
     * @type {string}
     * @memberof AppealRequest
     */
    visit_at?: string;
    /**
     * Удобный промежуток времени визита
     * @type {string}
     * @memberof AppealRequest
     */
    visit_time_range?: string;
    /**
     * Дата/время плановое
     * @type {string}
     * @memberof AppealRequest
     */
    plan_at?: string;
    /**
     * Признак платной заявки
     * @type {boolean}
     * @memberof AppealRequest
     */
    is_pay?: boolean;
    /**
     * Признак аварийной заявки
     * @type {boolean}
     * @memberof AppealRequest
     */
    is_crash?: boolean;
    /**
     * Признак гарантийной заявки
     * @type {boolean}
     * @memberof AppealRequest
     */
    is_guarantee?: boolean;
    /**
     * Этаж
     * @type {number}
     * @memberof AppealRequest
     */
    floor?: number;
    /**
     * Подъезд
     * @type {number}
     * @memberof AppealRequest
     */
    entrance?: number;
    /**
     * Email
     * @type {string}
     * @memberof AppealRequest
     */
    email?: string;
    /**
     * Стоимость услуги
     * @type {string}
     * @memberof AppealRequest
     */
    sum?: string;
    /**
     * Оценка
     * @type {string}
     * @memberof AppealRequest
     */
    rating?: string;
    /**
     * Комментарий к выполнению заявки
     * @type {string}
     * @memberof AppealRequest
     */
    rating_comment?: string;
    /**
     * Уникальный идентификатор вида работ
     * @type {number}
     * @memberof AppealRequest
     */
    appeal_job_id?: number;
    /**
     * Уникальный идентификатор объекта заявки
     * @type {number}
     * @memberof AppealRequest
     */
    appeal_place_id?: number;
    /**
     * Причина отмены заявки
     * @type {string}
     * @memberof AppealRequest
     */
    reason?: string;
    /**
     * Статус
     * @type {number}
     * @memberof AppealRequest
     */
    status?: number;
    /**
     * Статус оплаты
     * @type {string}
     * @memberof AppealRequest
     */
    pay_status?: string;
    /**
     *
     * @type {Array<number>}
     * @memberof AppealRequest
     */
    files?: Array<number>;
}
