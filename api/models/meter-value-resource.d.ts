/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MeterValueResource
 */
export interface MeterValueResource {
    /**
     * Уникальный идентификатор счетчика
     * @type {number}
     * @memberof MeterValueResource
     */
    meter_id: number;
    /**
     * Показание основной шкалы
     * @type {number}
     * @memberof MeterValueResource
     */
    main_scale_value: number;
    /**
     * Показание дополнительной шкалы (для двушкального счетчика)
     * @type {number}
     * @memberof MeterValueResource
     */
    vsp_scale_value?: number;
    /**
     * Переполнение основной шкалы (0 - нет, 1 - да)
     * @type {boolean}
     * @memberof MeterValueResource
     */
    repletion_main?: boolean;
    /**
     * Переполнение дополнительной шкалы (0 - нет, 1 - да)
     * @type {boolean}
     * @memberof MeterValueResource
     */
    repletion_vsp?: boolean;
}
