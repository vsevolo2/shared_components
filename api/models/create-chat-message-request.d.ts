/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { FilesUploadRequest } from './files-upload-request';
/**
 *
 * @export
 * @interface CreateChatMessageRequest
 */
export interface CreateChatMessageRequest {
    /**
     *
     * @type {Array<FilesUploadRequest>}
     * @memberof CreateChatMessageRequest
     */
    files?: Array<FilesUploadRequest>;
    /**
     * Сообщение
     * @type {string}
     * @memberof CreateChatMessageRequest
     */
    message?: string;
}
