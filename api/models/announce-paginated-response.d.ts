/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { Announce } from './announce';
import { Paginator } from './paginator';
/**
 *
 * @export
 * @interface AnnouncePaginatedResponse
 */
export interface AnnouncePaginatedResponse {
    /**
     *
     * @type {Array<Announce>}
     * @memberof AnnouncePaginatedResponse
     */
    items: Array<Announce>;
    /**
     *
     * @type {Paginator}
     * @memberof AnnouncePaginatedResponse
     */
    pagination: Paginator;
}
