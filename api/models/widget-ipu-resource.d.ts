/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Инфо по персональным счетчикам
 * @export
 * @interface WidgetIpuResource
 */
export interface WidgetIpuResource {
    /**
     * Первый день сдачи показаний ИПУ
     * @type {number}
     * @memberof WidgetIpuResource
     */
    from?: number;
    /**
     * Последний день сдачи показаний ИПУ
     * @type {number}
     * @memberof WidgetIpuResource
     */
    to?: number;
}
