/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MeterDeviceType
 */
export interface MeterDeviceType {
    /**
     * Уникальный идентификатор
     * @type {number}
     * @memberof MeterDeviceType
     */
    id: number;
    /**
     * Название
     * @type {string}
     * @memberof MeterDeviceType
     */
    name: string;
    /**
     * Ед. измерения
     * @type {string}
     * @memberof MeterDeviceType
     */
    unit: string;
    /**
     * Разрядность
     * @type {number}
     * @memberof MeterDeviceType
     */
    digits?: number;
    /**
     * Точность
     * @type {number}
     * @memberof MeterDeviceType
     */
    precision?: number;
    /**
     * Разрядность вспомогательной шкалы
     * @type {number}
     * @memberof MeterDeviceType
     */
    vsp_digits?: number;
    /**
     * Точность вспомогательной шкалы
     * @type {number}
     * @memberof MeterDeviceType
     */
    vsp_precision?: number;
}
