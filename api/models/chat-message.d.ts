/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { FileResource } from './file-resource';
import { Sender } from './sender';
/**
 *
 * @export
 * @interface ChatMessage
 */
export interface ChatMessage {
    /**
     * Уникальный идентификатор
     * @type {number}
     * @memberof ChatMessage
     */
    id: number;
    /**
     * Уникальный идентификатор чата
     * @type {string}
     * @memberof ChatMessage
     */
    chat_id: string;
    /**
     * Дата отправки сообщения
     * @type {string}
     * @memberof ChatMessage
     */
    created_at: string;
    /**
     * Сообщение
     * @type {string}
     * @memberof ChatMessage
     */
    message?: string;
    /**
     * Дата чтения
     * @type {string}
     * @memberof ChatMessage
     */
    read_at?: string;
    /**
     *
     * @type {Sender}
     * @memberof ChatMessage
     */
    sender: Sender;
    /**
     * Файлы
     * @type {Array<FileResource>}
     * @memberof ChatMessage
     */
    files: Array<FileResource>;
}
