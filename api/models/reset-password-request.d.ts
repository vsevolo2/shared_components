/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ResetPasswordRequest
 */
export interface ResetPasswordRequest {
    /**
     * Номер телефона
     * @type {string}
     * @memberof ResetPasswordRequest
     */
    phone: string;
    /**
     * пароль
     * @type {string}
     * @memberof ResetPasswordRequest
     */
    password: string;
}
