/**
 * ORS Api (Person)
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.0-alpha
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ChatMessage } from './chat-message';
/**
 *
 * @export
 * @interface ChatMessageResponse
 */
export interface ChatMessageResponse {
    /**
     *
     * @type {ChatMessage}
     * @memberof ChatMessageResponse
     */
    chat_message: ChatMessage;
}
