import { defineStore } from 'pinia';
import { DefaultApi, Announce as AnnounceApi} from "~/api";
import { useApi } from "~/composables/useApi";

interface ArticleResource {
  api: DefaultApi,
  announces: AnnounceApi[],
}

/**
 * Объявление / Новость
 */
export const Announce = defineStore('Announce', {
  state: (): ArticleResource => ({
    api: useApi(),
    announces: [],
  }),
  getters: {
  },
  actions: {
    async getAnnounce(id: number, reload?: boolean): Promise<AnnounceApi> {
      if (this.isLoaded(id) && !reload)
        return this.announces.find(p => p.id === id) ?? {} as AnnounceApi;
      const { data } = await this.$state.api.getAnnounce(id);
      const announce = data.announce as AnnounceApi;
      this.announces.push(announce);
      return announce;
    },
    setAnnounce(announce: AnnounceApi): void {
      this.clearAnnounce(announce.id);
      this.announces.push(announce);
    },
    clearAnnounce(id: number): void {
      this.announces = this.announces.filter(p => p.id !== id);
    },
    isLoaded(id: number): boolean {
      return !!this.announces.find(m => m.id === id);
    },
  }
});
