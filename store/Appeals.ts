import { defineStore } from 'pinia';
import {
  Appeal, ChatMessage,
  DefaultApi
} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import {AppealForm, steps} from "~/composables/appealHelper";

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

interface AppealTypeInterface extends collectionInterface {
  api: DefaultApi,
  filters: string[],
  filter: { key: string; value: string|string[] }[],
  items: Array<Appeal>,
  loadWidget: boolean
}

/**
 * Список заявок
 */
export const Appeals = defineStore('Appeals', {
  state: (): AppealTypeInterface => ({
    api: useApi(),
    pagination,
    filters: [
      'filter[number]',
      'filter[house_id]',
      'filter[flat_id]',
    ],
    filter: [],
    isChanged: false,
    sorts: ['id', 'created_at', 'updated_at', 'number'],
    sort_by: '-created_at',
    sort_order: -1,
    items: [],
    loaded: false,
    loadWidget: false
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    search: (store) => getFilterByKey(store, 'filter[number]')?.value ?? ""
  },
  actions: {
    async getList(page?: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      this.loadWidget = false;
      const { data } = await this.$state.api.getAppealList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          getFilterByKey(this.$state, 'filter[number]')?.value as string,
          getFilterByKey(this.$state, 'filter[house_id]')?.value as number,
          getFilterByKey(this.$state, 'filter[flat_id]')?.value as number,
          undefined,
          undefined
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.isChanged = false;
      this.loaded = true;
    },
    // загрузка заявки для виджета
    async getWidget(): Promise<void> {
      this.loadWidget = false;
      await this.getList(1, 1, '-updated_at');
      this.loadWidget = true
    },
    clearFilterByKey(key: string, value?: string): void {
      clearFilter(this.$state, key, value);
    },
    setSearch(value?: string): void {
      setFilter(this.$state, 'filter[number]', value);
    },
    getFilter(key: string): string|number|string[]|boolean {
      return getFilterByKey(this.$state, key)?.value ?? ""
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    },
    async getAppeal(id: number): Promise<Appeal> {
      const { data } = await this.$state.api.getAppeal(id);
      const appeal = data.appeal as Appeal;
      this.setAppealForm(appeal)
      return { ...appeal,  ...{ chat: {} as ChatMessage[] } };
    },
    findAppeal(id: number): Appeal {
      return this.items.find(p => p.id === id) ?? {} as Appeal;
    },
    setAppealForm(appeal: Appeal): void {
      if (!this.isLoaded(appeal.id)) {
        this.items.push(appeal);
      }
    },
    isLoaded(id: number): boolean {
      return !!(this.findAppeal(id))
    }

  }
});
