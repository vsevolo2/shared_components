import { defineStore } from 'pinia';
import { City, DefaultApi, Paginator, Street} from "~/directory";
import { useDirectoryApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const {
  pagination,
  setDefaultCollection,
} = useCollectionStore();

interface CityType extends collectionInterface  {
  api: DefaultApi,
  pagination: Paginator,
  loaded: boolean,
  items: City[]
}

/**
 * Список городов c пагинацией
 */
export const Cities = defineStore('Cities', {
  state: (): CityType => ({
    api: useDirectoryApi(),
    loaded: false,
    filter_default: {},
    pagination,
    sorts: ['id', 'name', 'city_id'],
    sort_by: 'name',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    getItems: (store) => store.items.map((i: any) => {
      return {
        value: i.id,
        text: i.name
      }
    })
  },
  actions: {
    async getList(page: number, per_page?: number, name?: string): Promise<void> {
      this.loaded = false;
      const { data } = await this.$state.api.getCityList(
          page,
          per_page ?? this.pagination.per_page,
          'name',
          name
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
