import { defineStore } from 'pinia';
import { DefaultApi, Paginator, Flat } from "~/directory";
import { useDirectoryApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import {Profile} from "~/store/Profile";

const {
  pagination,
  setDefaultCollection,
} = useCollectionStore();

/**
 * Список городов c пагинацией
 */
export const Flats = defineStore('Flats', {
  state: (): any => ({
    api: useDirectoryApi(),
    user: Profile(),
    loaded: false,
    filter_default: {
      house_id: '',
      filter_number: ''
    },
    pagination,
    sorts: ['id', 'name'],
    sort_by: 'name',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    getItems: (store) => store.items.map((i: any) => {
      return {
        value: i.id,
        text: i.number
      }
    })
  },
  actions: {
    async getList(page?: number, per_page?: number): Promise<void> {
      const { data } = await this.$state.api.getFlatList(
          this.user.groupId,
          page,
          per_page ?? this.pagination.per_page,
          'id', // sort
          this.$state.filter_default?.house_id,
          this.$state.filter_default?.filter_number
      );
      this.pagination = data.pagination;
      this.items = data.items;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
