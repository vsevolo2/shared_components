import { defineStore } from 'pinia';
import { DefaultApi, Paginator, Street } from "~/directory";
import { useDirectoryApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";

const { pagination, getFilterByKey } = useCollectionStore();

interface StreetsType extends collectionInterface  {
  api: DefaultApi,
  pagination: Paginator,
  items: Street[]
}

/**
 * Справочник улиц c пагинацией
 */
export const Streets = defineStore('Streets', {
  state: (): StreetsType => ({
    api: useDirectoryApi(),
    loaded: false,
    pagination,
    filter_default: {
      city_id: ''
    },
    sorts: ['id', 'name'],
    sort_by: 'name',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    getItems: (store) => store.items.map((i: Street) => {
      return {
        value: i.id,
        text: i.name
      }
    })
  },
  actions: {
    async getList(page = 1, per_page = 200, filter_name?: string): Promise<void> {
      this.loaded = false;
      const { data } = await this.$state.api.getStreetList(
          page,
          per_page ?? this.pagination.per_page,
          'name', // sorting
          filter_name, // [filter_name] Фильтр по названию улицы
          undefined, // [filter_id] Фильтр по id улицы
          Number(this.$state.filter_default?.city_id)
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
