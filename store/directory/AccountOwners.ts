import { defineStore } from 'pinia';
import { DefaultApi, PersonalAccount } from "~/directory";
import { useDirectoryApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { Profile } from "~/store/Profile";

const {
  pagination,
  getSortRecords,
} = useCollectionStore();

interface AccountOwnersType extends collectionInterface {
  api: DefaultApi,
  user: Record<string, any>,
  filter_default: Record<string, string>,
  items: PersonalAccount[]
}

/**
 * Список ЛС c пагинацией
 */
export const AccountOwnerDirectory = defineStore('AccountOwnerDirectory', {
  state: (): AccountOwnersType => ({
    api: useDirectoryApi(),
    user: Profile(),
    loaded: false,
    pagination,
    filter_default: {
      id: '',
      account: '',
      house_id: '',
      flat_id: '',
      flat_number: ''
    },
    sorts: ['id', 'name'],
    sort_by: 'name',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    getItems: (store) => store.items.map((i: PersonalAccount) => {
      return {
        value: i.id,
        text: i.account
      }
    })
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      this.loaded = false;
      const flatFilter = this.$state.filter_default?.flat_id ?? undefined;
      const elemSort = getSortRecords(this.$state, sort);
      const { data } = await this.$state.api.getPersonalAccountList(
          this.user.groupId,
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          Number(this.$state.filter_default?.account) ?? undefined,
          this.$state.filter_default?.house_id,
          Number(flatFilter),
          undefined,
          1, //filter_status
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
