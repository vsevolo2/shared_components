import { defineStore } from 'pinia';
import { useStorage } from '@vueuse/core'

export const invitationStore = defineStore('invitation', {
  state: () => ({
    unlock_time: useStorage('invitation_unlock_time', 0),
    forcedShow: false
  }),
  getters: {
    show_prompt_install: (state) => {
      return ((new Date).getTime()) > state.unlock_time;
    }
  },
  actions: {
    stop_install_app() {
      this.unlock_time = (new Date).getTime() + 1000*60*60*24*7; // 7days
    },
  },

});
