import {defineStore} from 'pinia';
import {useStorage} from '@vueuse/core'

export const UserDevice = defineStore('UserDevice', {
    state: () => ({token: useStorage('user_device_token', '')}),
    actions: {
        register(token: string) {
            // отправлять на сервер нужно если есть токен и он отличается от того,
            // что мы ранее отправляли на сервер
            const shouldSendToBackend = token && token != this.token

            // console.log('register', shouldSendToBackend, token);

            this.token = token;
            shouldSendToBackend && useApi().registerDevice({token});
        },
        unregister() {
            const token = this.token
            // отправка на сервер нужна только в одном случае - у нас был токен
            const shouldSendToBackend = !!token

            // console.log('unregister', shouldSendToBackend, token);

            this.token='';
            shouldSendToBackend && useApi().unregisterDevice({token});
        },
    }
});
