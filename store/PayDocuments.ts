import { defineStore } from 'pinia';
import {Paginator, DefaultApi, Epd } from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, {collectionInterface, sortInterface} from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

interface PayDocumentsType extends collectionInterface  {
  api: DefaultApi,
  pagination: Paginator,
  loaded: boolean,
  items: Array<Epd>
}

/**
 * Список платежных документов
 */
export const PayDocuments = defineStore('PayDocuments', {
  state: (): PayDocumentsType => ({
    api: useApi(),
    loaded: false,
    sorts: ['id', 'period_date'],
    sort_by: '-id',
    sort_order: -1,
    pagination,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getEpdList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          undefined
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
