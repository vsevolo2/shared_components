import { defineStore } from 'pinia';
import {Paginator, DefaultApi, AppealJob} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

interface AppealJobsType extends collectionInterface {
  api: DefaultApi,
  filter_default: Record<string, string>
  pagination: Paginator,
  loaded: boolean,
  items: Array<AppealJob>
}

/**
 * Список счетчиков c пагинацией
 */
export const AppealJobs = defineStore('AppealJobs', {
  state: (): AppealJobsType => ({
    api: useApi(),
    loaded: false,
    filter_default: {
      category_id: ''
    },
    pagination,
    sorts: ['id'],
    sort_by: 'id',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page?: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      this.loaded = false;
      const { data } = await this.$state.api.getAppealJobCollection(
          undefined,  //page
          undefined,  //per_page
          undefined,  //sort
          undefined,  //filter_name
          Number(this.$state.filter_default?.category_id)
      )
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
