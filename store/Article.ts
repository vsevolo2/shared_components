import { defineStore } from 'pinia';
import { DefaultApi, Article as ArticleApi} from "~/api";
import { useApi } from "~/composables/useApi";

interface ArticleResource {
  api: DefaultApi,
  articles: ArticleApi[],
}

/**
 * Категория статей
 */
export const Article = defineStore('Article', {
  state: (): ArticleResource => ({
    api: useApi(),
    articles: [],
  }),
  getters: {
  },
  actions: {
    async getArticle(id: number, reload?: boolean): Promise<ArticleApi> {
      if (this.isLoaded(id) && !reload)
        return this.articles.find(p => p.id === id) ?? {} as ArticleApi;
      const { data } = await this.$state.api.getArticle(id);
      const article = data.article as ArticleApi;
      this.articles.push(article);
      return article;
    },
    setArticle(article: ArticleApi): void {
      this.clearArticle(article.id);
      this.articles.push(article);
    },
    clearArticle(id: number): void {
      this.articles = this.articles.filter(p => p.id !== id);
    },
    isLoaded(id: number): boolean {
      return !!this.articles.find(m => m.id === id);
    },
  }
});
