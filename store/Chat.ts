import { defineStore } from 'pinia';
import {
  ChatMessage,
  DefaultApi
} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

interface ChatTypeInterface extends collectionInterface {
  api: DefaultApi,
  items: Array<ChatMessage>
}

/**
 * Чат по зяавке
 */
export const Chat = defineStore('Chat', {
  state: (): ChatTypeInterface => ({
    api: useApi(),
    pagination,
    sorts: [],
    sort_by: '-created_at',
    sort_order: -1,
    items: [],
    loaded: false
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
  },
  actions: {
    async getList(appeal: any, page?: number, per_page?: number, sort?: string): Promise<void> {
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getChatMessageList(
          appeal.chat_id,
          page,
          per_page ?? this.pagination.per_page,
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
      appeal.chat = this;
    },
    clearFilterByKey(key: string, value?: string): void {
      clearFilter(this.$state, key, value);
    },
    getFilter(key: string): string|number|string[]|boolean {
      return getFilterByKey(this.$state, key)?.value ?? ""
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
