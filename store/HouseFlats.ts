import {defineStore} from 'pinia';
import {HouseFlat} from '~/api';
import {HouseFlatSorterFunc} from '~/utils';

interface HouseFlatsState
{
  loaded: boolean,
  filter_house_id: string,
  items_house_id: string,
  items: HouseFlat[]
  cache: Record<string, HouseFlat[]>
}

/**
 * Подгрузка помещений
 */
export const HouseFlats = defineStore('HouseFlats', {
  state: (): HouseFlatsState => ({
    loaded: false,
    filter_house_id: '',
    items_house_id: '',
    items: [],
    cache: {}
  }),
  getters: {
    getItems: (store) => store.items.map((flat: HouseFlat) => {
      return {
        value: flat.id,
        text: flat.number
      };
    })
  },
  actions: {
    async getList(): Promise<void> {
      // если у нас уже загружены квартиры этого дома - останавливаемся
      if (this.filter_house_id === this.items_house_id)
      {
        return;
      }

      if (!this.filter_house_id)
      {
        this.items = [];
        this.items_house_id = this.filter_house_id;
        return;
      }

      this.loaded = false;

      if (!this.cache.hasOwnProperty(this.filter_house_id))
      {
        const {data} = await useApi().autocompleteFlats(this.filter_house_id);
        this.cache[this.filter_house_id] = data.items.sort(HouseFlatSorterFunc);
      }

      this.items = this.cache[this.filter_house_id];
      this.items_house_id = this.filter_house_id;

      this.loaded = true;
    },
    setHouseId(house_id: string): void {
      this.filter_house_id = house_id;
    }
  }
});
