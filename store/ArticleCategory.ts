import { defineStore } from 'pinia';
import { DefaultApi, ArticleCategory as ArticleCategoryApi, Paginator, Article } from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, {sortInterface} from '~/composables/useCollectionStore';
import { LocationQuery } from 'vue-router';

export interface ArticleCategoryResource extends ArticleCategoryApi {
  listArticles: {
    pagination: Paginator,
    loaded: boolean,
    items: Article[]
  } & sortInterface,
}

interface ArticleCategoryType {
  api: DefaultApi,
  categories: ArticleCategoryResource[],
}
const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

/**
 * Категория статей
 */
export const ArticleCategory = defineStore('ArticleCategory', {
  state: (): ArticleCategoryType => ({
    api: useApi(),
    categories: [],
  }),
  getters: {
  },
  actions: {
    async getArticleByCategory(articleCategory: ArticleCategoryApi): Promise<ArticleCategoryResource> {
      const category = this.categories.find(p => p.id === articleCategory.id);
      if (category)
        return category;

      const articleCategoryResource = articleCategory as ArticleCategoryResource;
      articleCategoryResource.listArticles = {
        items: [],
        loaded: false,
        pagination,
        sorts: ['id', 'pos'],
        sort_by: 'pos',
        sort_order: 1,
      };
      await this.getArticlesList(articleCategoryResource, 1, 100);
      this.categories.push(articleCategoryResource);
      return articleCategoryResource;
    },
    isLoaded(id: number): boolean {
      return !!this.categories.find(m => m.id === id);
    },
    init(elem: { pagination: Paginator } & sortInterface, query?: LocationQuery): void {
      setDefaultCollection(elem, query);
    },
    async getArticlesList(articleCategory: ArticleCategoryResource, page: number, per_page?: number, sort?: string, title?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(articleCategory.listArticles, page, articleCategory.listArticles.loaded, per_page, sort))
        return;
      articleCategory.listArticles.loaded = false;
      const {data} = await this.$state.api.getArticleList(
          articleCategory.id,
          page,
          per_page ?? articleCategory.listArticles.pagination.per_page,
          'pos',
          undefined,
          title,
          true
      );
      articleCategory.listArticles.pagination = data.pagination;
      articleCategory.listArticles.items = data.items;
      articleCategory.listArticles.loaded = true;
    },
  }
});
