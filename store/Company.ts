import { defineStore } from 'pinia';
import {Company, FlatAccount} from "~/api";

interface CompanyState{
  loaded: boolean,
  items: [],
}
/**
 * Справочник управляющей компании
 */
export const Company = defineStore('Company', {
  state: (): CompanyState => ({
    loaded: false,
    items: []
  }),
  getters: {
    getItems: (store) => store.items.map((company: Company) => {
      return company
    })
  },
  actions: {
    async getList(): Promise<void> {
      if (!this.loaded)
      {
        this.loaded = false;
        const {data} = await useApi().getCompany()
        this.items.push(data);
        this.loaded = true;
      }
    }
  }
});
