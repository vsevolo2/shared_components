import { defineStore } from 'pinia';
import {Paginator, DefaultApi, AppealPlace, PersonalAccount} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import {Profile} from "~/store/Profile";

const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

interface AppealObjectsType extends collectionInterface {
  api: DefaultApi,
  account: PersonalAccount,
  filter_default: Record<string, string>
  pagination: Paginator,
  loaded: boolean,
  items: Array<AppealPlace>
}

/**
 * Список объектов
 */
export const AppealObjects = defineStore('AppealObjects', {
  state: (): AppealObjectsType => ({
    api: useApi(),
    loaded: false,
    filter_default: {
      category_id: ''
    },
    // активный ЛС Жителя
    account: Profile().currentAccount() as PersonalAccount,
    pagination,
    sorts: ['id'],
    sort_by: 'id',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page?: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      this.loaded = false;
      const { data } = await this.$state.api.getAppealPlaceList(
          undefined,  //page
          undefined,  //per_page
          undefined,  //sort
          undefined,  //filter_id
          undefined,  //filter_name
      )
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
