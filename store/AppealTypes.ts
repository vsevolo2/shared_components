import { defineStore } from 'pinia';
import {Paginator, DefaultApi, AppealType, PersonalAccount} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import {Profile} from "~/store/Profile";

const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

interface AppealTypes extends collectionInterface {
  api: DefaultApi,
  filter_default: Record<string, string>
  pagination: Paginator,
  loaded: boolean,
  items: Array<AppealType>
}

/**
 * Список счетчиков c пагинацией
 */
export const AppealTypes = defineStore('AppealTypes', {
  state: (): AppealTypes => ({
    api: useApi(),
    loaded: false,
    filter_default: {},
    pagination,
    sorts: ['id'],
    sort_by: 'id',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page?: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getAppealTypeCollection(
          false,
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
