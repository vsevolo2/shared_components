import { defineStore } from 'pinia'
import {
  DefaultApi,
  Group,
  ProfileResource,
  PersonPersonalAccountsResource,
  WidgetDashboard,
  PersonalAccountAddress,
  GroupBrandingPerson
} from "~/api";
import { brandingOptions } from "~/composables/useConst";
import { useApi } from '~/composables/useApi';
import { Payment } from "~/store/Payment";
import { useLocal } from "~/composables/useLocal";
import { addressShort } from "~/composables/stringHelper";
import {Card} from "../api";

type BrandingKeys = keyof GroupBrandingPerson

interface ProfileType {
  api: DefaultApi,
  loaded: boolean,
  groupLoaded: boolean,
  noHasGroup: boolean,
  widget?: WidgetDashboard,
  paymentItems?: Array<PersonPersonalAccountsResource>,
  options: GroupBrandingPerson,
  profile: ProfileResource,
  group: Group,
  accountId: number | undefined,
  reset_password_token: string | undefined,
  file_max_size: string | undefined,
  cards: Card[] | undefined,
  showDeleteMessage: boolean
}

/**
 * Данные учетной записи
 */
export const Profile = defineStore('Profile', {
  state: (): ProfileType => ({
    api: useApi(),
    paymentItems: Payment().items,
    loaded: false,
    // цветовая гамма ЛК по умолчанию
    options: Object.assign({}, brandingOptions) as GroupBrandingPerson,
    reset_password_token: undefined,
    file_max_size: undefined,
    cards: undefined,
    profile: {
      phone: '',
      accounts: [] as Array<PersonalAccountAddress>,
      unread_messages: 0,
    } as ProfileResource,
    group: {} as Group,
    accountId: undefined,
    widget: {} as WidgetDashboard,
    groupLoaded: false,
    noHasGroup: false,
    showDeleteMessage: false,
  }),
  getters: {
    data: (store): ProfileResource => store.profile,
    getShowDeleteMessage: (store): boolean => store.showDeleteMessage,
    name: (store): string => [store.profile.first_name].join(" ").trim(),
    isNameRequired(): boolean {
      return this.name.length === 0
    },
    accounts: (store): PersonalAccountAddress[]  => store.profile.accounts,
    fio: (store): string  => [store.profile.last_name, store.profile.first_name, store.profile.middle_name].join(" ").trim(),
    fullName: (store): string => {
      const name = [store.profile.last_name, store.profile.first_name, store.profile.middle_name].join(" ");
      if (name.trim().length === 0) {
        return String(store.profile.phone);
      }
      return name;
    },
    isLoaded: (store): Boolean => store.loaded, // была попытка загрузить профиль пользователя
  },
  actions: {
    setShowDeleteMessage(val: boolean): undefined {
        this.showDeleteMessage = val;
    },
    // получение информации о группе пользователя
    async getGroup(): Promise<void> {
      const { data } = await this.$state.api.getGroup();
      if (!data)
        return ;
      // сохранение цветовой гаммы ЛК
      this.setBrandingOptions(data.group.branding_person);

      // const x  = Object.assign({}, data.group.branding_person || {})
      // setTimeout(() => {
      //   x.backgroundColor = undefined
      //   x.headerColor = undefined
      //   x.headerBgColor = 'yellow'
      //   x.headerBottomBgColor = 'green'
      //   x.pageTitleColor = undefined
      //   x.headerLogoTitle = 'ДЕмо'
      //   this.setBrandingOptions(x);
      // }, 4000)

      this.group = data.group;
      // килобайты в байты
      if (data.group.file_max_size) {
        this.file_max_size = String(parseInt(data.group.file_max_size as string) * 1024);
      }
    },
    async uploadPhoto(file: File): Promise<void> {
      const { data } = await this.$state.api.uploadProfilePhotoForm(file);
      this.profile.photo = data.photo;
    },
    async deletePhoto(): Promise<void> {
      await this.$state.api.destroyProfilePhoto();
      this.profile.photo = undefined;
    },
    async getWidgetDashboard(): Promise<void> {
      const { data } = await this.$state.api.getWidgetDashboard();
      this.widget = data;
    },
    async getProfile(): Promise<void> {
      const { data } = await this.$state.api.getProfile();
      this.profile = this.setProfile(data.profile);
      this.loaded = true;
    },
    async getCards(): Promise<void> {
      const { data } = await this.api.getCards();
      this.cards = data.items as Card[];
    },
    setProfile(profile: ProfileResource): ProfileResource {
      profile.accounts = profile.accounts.map((item: PersonalAccountAddress) => {
        const { address, flat } = addressShort(item.address);
        return {
          ...item,
          ...{ short_address: address + ', ' + flat }
        }
      })
      return profile;
    },
    setAccount(account:PersonalAccountAddress): void {
      this.accountId = account.id
    },
    setBrandingOptions(options: GroupBrandingPerson | undefined): void {
      let opts = Object.assign({}, brandingOptions) as GroupBrandingPerson;

      for (const [key, value] of Object.entries(options || {})) {
        opts[<BrandingKeys>key] = value
      }
      this.options = opts
    },
    accountById(accountId: number | string): PersonalAccountAddress | undefined {
      return this.profile.accounts.find((account: PersonalAccountAddress) => account.id === Number(accountId))
    },
    accountsAreNotEmpty(): boolean {
     return this.accounts.length > 0
    },
    currentAccount(debug?:boolean): PersonalAccountAddress | undefined {
      const accountId = this.accountId ? this.accountId : useLocal().get();

      return accountId && this.accountsAreNotEmpty()
          ? this.accountById(accountId)
          : undefined;
    },
    appealBlocked(): boolean {
      return this.currentAccount()?.appeal_blocked ?? false;
    },
    inputIpuBlocked(): boolean {
      return this.currentAccount()?.input_ipu_blocked ?? false;
    },
    decreaseUnreadMessages(): void {
        this.profile.unread_messages--;
    },
    resetUnreadMessages(): void {
      this.profile.unread_messages = 0;
    },
  }
});
