import { defineStore } from 'pinia';
import {
  BasketItemResource,
  BasketResource,
  BasketResourceGroup,
  DefaultApi,
  PersonalAccountAddress,
  PersonPersonalAccountsResource
} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, {collectionInterface} from "~/composables/useCollectionStore";
import {Profile} from "~/store/Profile";

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

export interface serviceItem {
  checked: boolean;
  company: string;
  items: PersonAccountsResourceItems[];
  key: number;
  price: number;
  total: number;
  totalFee: number;
}
interface BasketItem extends BasketItemResource {
  price:number
  debts: number;
  priceState?: number;
  enableState?: boolean;
  service_company: string;
  fee: number;
  calculatedFee: number;
}

interface PaymentItem extends PersonPersonalAccountsResource {
  account?: string;
  services: Record<string, serviceItem>;
}

interface  PayDocumentsType extends collectionInterface {
  api: DefaultApi,
  items: PersonAccountsResourceItems[],
  historyItems?: BasketResource[],
  services: Record<string, serviceItem>,
  historyLoaded: boolean
}

export interface PersonAccountsResourceItems extends PersonPersonalAccountsResource {
  calculatedFee: number;
  debts: number;
  enabled:boolean;
  fee: number;
  gen_index: string;
  max_price: number;
  not_editable: boolean;
  price: number;
  priceState?: number | string;
  enableState?: boolean;
  appeal_number?: string;
  service: string;
  service_company: string;
  service_id?: number;
  service_name:string;
  errors?: string;
}

export default interface PayDocumentExtension extends PayDocumentsType {
  readonly address?: string;
  readonly id: number;
  account?: string;
  price: number
  accountPrice: number;
  f_checked: boolean;
  paymentHidden?: boolean;
  service_id: number;
  loading?: boolean;
  errors?: {};
  enabled:boolean;
}
/**
 * Платежи
 */
export const Payment = defineStore('Payment', {
  state: (): PayDocumentsType => ({
    api: useApi(),
    pagination,
    items: [],
    historyItems: [],
    services: {},
    sorts: [],
    sort_by: '-created_at',
    sort_order: -1,
    historyLoaded: false
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isLoaded: (store) => store.loaded,
    isEmpty: (store) => !store.items?.length && store.loaded
  },
  actions: {
    async getPaymentHistory(page: number, per_page?: number): Promise<void> {
      const { data } = await this.api.getPaymentHistory(page, per_page);
      this.historyItems = data.items?.map((item: BasketResource) => {
        item.groups = this.setPayment(item.groups as Array<BasketResourceGroup>);
        return item
      });
      this.pagination = data.meta;
      this.historyLoaded = true;
    },
    async getPaymentAccounts(): Promise<void> {
      const { data } = await this.api.getPaymentAccounts();
      this.items = this.setPayment(data.items as Array<PersonAccountsResourceItems>);
    },
    // проверка всех сервисов одного платежа
    servicesCheck(group: PaymentItem): boolean {
      const isMain =  useRoute().query.main;
      const currentAccount = Profile().currentAccount() as PersonalAccountAddress;

      const checkedServices = [] as Array<boolean>;
      // services list
      Object.keys(group.services).forEach((serviceName: string) => {
        let serviceCheck = false;
        const service = group.services[serviceName];

        service.items.forEach((item: BasketItemResource & { enabled: boolean }) => {
          if (isMain && currentAccount.account !== group.account) {
            item.enabled = false;
          }
          if (item.enabled)
            serviceCheck = true;
        });
        checkedServices.push(serviceCheck)

      });
      // если хоть один сервис валиден, возвращаем true
      return !!checkedServices.find(check => check);
    },
    setPayment(items: Array<PersonPersonalAccountsResource>): Array<PersonPersonalAccountsResource> {
      return items.map((account: PersonPersonalAccountsResource) => {
        let price: number = 0;
        let services: Record<string, any> = {};
        let serviceName: string = '';
        let key: number = 0;
        account.items?.map((item: BasketItemResource) => {
          const newItem = { ...item } as BasketItem;
          newItem.debts = item.price as number;
          newItem.priceState = item.price as number;
          const service = item.service as string
          serviceName = service;
          newItem.fee = parseFloat(String(item.part_com));

          if (!services[service]) {
            services[service] = {};
            services[service].items = [];
            services[service].price = 0;
            services[service].total = 0;
            services[service].company = '';
            services[service].key = key++;
          }
          services[service]?.items.push(newItem);
          services[service].price = services[service].price + Number(newItem.price);
          services[service].company = newItem.service_company;
          services[service].total += Number(item.price);
          price += Number(item.price);
        })

        account = {
          ...account,
          ...{ accountPrice: price },
          ...{ services: services },
        }

        return account;
      })
    },
  }
});
