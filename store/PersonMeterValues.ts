import { defineStore } from 'pinia';
import { Paginator, DefaultApi, PersonMeterValue } from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import {useLocal} from "~/composables/useLocal";
import Local from "~/plugins/api/Local";

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

interface PersonMeterValuesType extends collectionInterface {
  api: DefaultApi,
  local: Local,
  filters: string[],
  filter: { key: string; value: string|string[] }[],
  items: PersonMeterValue[]
}

/**
 * Список показаний c пагинацией
 */
export const PersonMeterValues = defineStore('PersonMeterValues', {
  state: (): PersonMeterValuesType => ({
    api: useApi(),
    loaded: false,
    local: useLocal(),
    filter_default: {},
    pagination,
    filters: [
      'filter[personal_account_id]',
      'filter[person_meter_id]',
      'filter[service_id]'
    ],
    filter: [],
    sorts: ['id', 'period_date', 'reading_date'],
    sort_by: '-reading_date',
    sort_order: 1,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      const accountId = !!Number(this.$state.local?.get()) ? Number(this.$state.local?.get()) : undefined;
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getPersonMeterValueList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          getFilterByKey(this.$state, 'filter[person_meter_id]')?.value as number, // Фильтр по id счетчика ИПУ
          undefined,  // Фильтр по дате ввода показания,
          getFilterByKey(this.$state, 'filter[service_id]')?.value as number, // Фильтр по id значения Услуги
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    setSearch(value?: string): void {
      setFilter(this.$state, 'filter[series]', value);
    },
    clearFilterByKey(key: string, value?: string): void {
      clearFilter(this.$state, key, value);
    },
    getFilter(key: string): string|number|string[] {
      return getFilterByKey(this.$state, key)?.value ?? ""
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
