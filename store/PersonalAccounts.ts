import { defineStore } from 'pinia';
import { DefaultApi, PersonalAccount} from "~/api";
import { useApi } from "~/composables/useApi";
import { useLocal } from "~/composables/useLocal";
import Local from "~/plugins/api/Local";
import { addressShort} from "~/composables/stringHelper";

export interface PersonalAccountType extends PersonalAccount{
  short_address?: string
  isActive?: boolean
}
interface PersonalAccountsType {
  api: DefaultApi,
  local: Local,
  loaded: boolean,
  items: PersonalAccountType[]
}

/**
 * Список ЛС c пагинацией
 */
export const PersonalAccounts = defineStore('PersonalAccounts', {
  state: (): PersonalAccountsType => ({
    api: useApi(),
    local: useLocal(),
    loaded: false,
    items: []
  }),
  getters: {
    list: (store) => store.items,
    hasAccounts: (store) => store.items.length > 0,
    isEmpty: (store) => !store.items.length && store.loaded,
  },
  actions: {
    async getList(reload = false): Promise<void> {
      if (this.loaded && !reload)
        return;
      const { data } = await this.$state.api.getAccountList();
      this.items = this.setAccountData(data.items)
      this.loaded = true;
    },
    // обработка аккаунта, дополнительные поля
    setAccountData(data: PersonalAccount[]): PersonalAccount[] {
      return data.map((item: PersonalAccount) => {
        const { address } = addressShort(item?.address as string);
        return {
          ...item,
          ...{ short_address: address + ', ' + item?.flat?.number }
        }
      })
    },
    // загрузка одиночного аккаутна
    async getAccount(id: number, reload?: boolean): Promise<PersonalAccount> {
      if (this.accountLoaded(id) && !reload)
        return this.findAccount(id) as PersonalAccount;
      const { data } = await this.$state.api.getAccount(id);
      this.setAccount(data.personal_account)
      return data.personal_account;
    },
    setAccount(account: PersonalAccount){
      let existReport = this.findAccount(account.id);
      // если есть перезаписываем данные
      if (existReport) {
        existReport = Object.assign(existReport, account);
        return;
      }
      this.items.push(account);
    },
    findAccount(id: number): PersonalAccount | undefined {
      return this.items.find(g => g.id === id) ?? undefined;
    },
    accountLoaded(id: number): boolean {
      return !!(this.findAccount(id))
    },
    currentAccount(): PersonalAccount | undefined {
      const accountId = this.$state.local?.get() ?? undefined;
      return accountId ? this.findAccount(Number(accountId)) : undefined;
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
