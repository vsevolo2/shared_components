import { defineStore } from 'pinia';
import {FlatAccount} from "~/api";

interface HouseFlatsState {
  loaded: boolean,
  filter_flat_id: string,
  flat_id: string,
  items: FlatAccount[]
  cache: Record<string, FlatAccount[]>
}

/**
 * Подгрузка ЛС
 */
export const FlatAccounts = defineStore('FlatAccounts', {
  state: (): HouseFlatsState => ({
    loaded: false,
    filter_flat_id: '',
    flat_id: '',
    items: [],
    cache: {}
  }),
  getters: {
    getItems: (store) => store.items.map((flatAccount: FlatAccount) => {
      return {
        value: flatAccount.id,
        text: flatAccount.account
      }
    })
  },
  actions: {
    async getList(): Promise<void> {
      // если у нас уже загружены ЛС для квартиры
      if (this.filter_flat_id === this.flat_id)
      {
        return;
      }

      if (!this.filter_flat_id)
      {
        this.items = [];
        this.flat_id = this.filter_flat_id;
        return
      }

      this.loaded = false;

      if (!this.cache.hasOwnProperty(this.filter_flat_id))
      {
        const { data } = await useApi().autocompleteAccounts(Number(this.filter_flat_id))
        this.cache[this.filter_flat_id] = data.items
      }

      this.items = this.cache[this.filter_flat_id];
      this.flat_id = this.filter_flat_id;

      this.loaded = true;
    },
    setFlatId(flat_id: string): void {
      this.filter_flat_id = flat_id;
    }
  }
});
