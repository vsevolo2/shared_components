import { defineStore } from 'pinia';
import {Paginator, DefaultApi, PersonMeter, MeterValueResource, PersonMeterValue, Service, BanPersonMeterValue} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const { pagination, isExistRecords, setDefaultCollection, getSortRecords } = useCollectionStore();

export interface PersonMeterForm extends PersonMeter {
  id: number,
  last_value?: PersonMeterValue,
  main_scale_value?: string,
  vsp_scale_value?: string,
  digits?: number,
  precision?: number,
  vsp_digits?: number,
  vsp_precision?: number,
  is_vsp: boolean,
  sended: boolean,
  check?: boolean,
  repletion_main: number,
  repletion_vsp: number,
  ban?: BanPersonMeterValue
  can_fill_value: boolean
}

interface PersonMetersType extends collectionInterface {
  api: DefaultApi,
  pagination: Paginator,
  loaded: boolean,
  items: Array<PersonMeter>,
  services: Array<Service>
}

/**
 * Список счетчиков c пагинацией
 */
export const PersonMeters = defineStore('PersonMeters', {
  state: (): PersonMetersType => ({
    api: useApi(),
    loaded: false,
    pagination,
    filter_default: {
      meter_status: ''  // упрощенные фильтры для справочников
    },
    sorts: ['id'],
    sort_by: 'id',
    sort_order: 1,
    items: [],
    services: []
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getPersonMeterList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          undefined, // service_id
          undefined, //  status meter_device
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.fillServices();
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    fillServices(): void {
      this.items.map(meter => {
        const foundService = this.services.find(service => service.id === meter.service?.id)
        if (!foundService) {
          this.services.push(meter.service as Service)
        }
      })
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
