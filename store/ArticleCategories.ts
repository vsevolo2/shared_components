import { defineStore } from 'pinia';
import { DefaultApi, ArticleCategory } from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';
import { Profile } from '~/store/Profile';

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
} = useCollectionStore();

interface ArticleCategoryType extends collectionInterface {
  api: DefaultApi,
  items: ArticleCategory[],
  filters: string[],
  filter: { key: string; value: string|string[] }[],
  search: string
}

/**
 * Список категорий заявки
 */
export const ArticleCategories = defineStore('ArticleCategories', {
  state: (): ArticleCategoryType => ({
    api: useApi(),
    loaded: false,
    pagination,
    filters: [],
    filter: [],
    isChanged: false,
    sorts: ['id', 'name'],
    sort_by: 'name',
    sort_order: 1,
    items: [],
    search: ''
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const user = Profile();
      this.loaded = false;
      const { data } = await this.$state.api.getArticleCategoryList(
          user.group?.id || 0,
          page,
          per_page ?? this.pagination.per_page,
          'name'
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.isChanged = false;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
