import { defineStore } from 'pinia';
import { DefaultApi, AnnounceCategory } from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

interface AnnounceCategoryType extends collectionInterface {
  api: DefaultApi,
  items: AnnounceCategory[],
  filters: string[],
  filter: { key: string; value: string|string[] }[],
}

/**
 * Список категорий объявлений
 */
export const AnnounceCategories = defineStore('AnnounceCategories', {
  state: (): AnnounceCategoryType => ({
    api: useApi(),
    loaded: false,
    pagination,
    filters: [
      'filter[name]',
    ],
    filter: [],
    isChanged: false,
    sorts: ['id', 'name'],
    sort_by: 'name',
    sort_order: 1,
    items: [],
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    search: (store) => getFilterByKey(store, 'filter[name]')?.value ?? ""
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getAnnounceCategoryList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          getFilterByKey(this.$state, 'filter[name]')?.value as string,
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.isChanged = false;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    setSearch(value?: string): void {
      setFilter(this.$state, 'filter[name]', value);
    },
    clearFilterByKey(key: string, value?: string): void {
      clearFilter(this.$state, key, value);
    },
    getFilter(key: string): string|number|string[]|boolean {
      return getFilterByKey(this.$state, key)?.value ?? ""
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
