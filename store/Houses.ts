import { defineStore } from 'pinia';
import { HouseAddress } from '~/api';

interface HouseState{
  loaded: boolean,
  items: HouseAddress[]
}

/**
 * Справочник домов
 */
export const Houses = defineStore('Houses', {
  state: (): HouseState => ({
    loaded: false,
    items: []
  }),
  getters: {
    getItems: (store) => store.items.map((house: HouseAddress) => {
      return {
        value: house.id,
        text: house.address
      }
    }),
  },
  actions: {
    async getList(): Promise<void> {
      if (!this.loaded)
      {
        this.loaded = false;
        const { data } = await useApi().autocompleteHouses()
        this.items = data.items;
        this.loaded = true;
      }
    }
  }
});
