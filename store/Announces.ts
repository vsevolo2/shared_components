import { defineStore } from 'pinia';
import {DefaultApi, Announce, PersonalAccount, PersonalAccountAddress} from "~/api";
import { useApi } from "~/composables/useApi";
import useCollectionStore, { collectionInterface } from "~/composables/useCollectionStore";
import { LocationQuery } from 'vue-router';

const {
  pagination,
  isExistRecords,
  setDefaultCollection,
  getSortRecords,
  getFilterByKey,
  setFilter,
  clearFilter
} = useCollectionStore();

interface ArticleType extends collectionInterface {
  api: DefaultApi,
  items: Announce[],
  filters: string[],
  filter: { key: string; value: string|string[] }[],
}

/**
 * События
 */
export const Announces = defineStore('Announces', {
  state: (): ArticleType => ({
    api: useApi(),
    loaded: false,
    pagination,
    filters: [
      'filter[title]',
      'filter[preview]',
      'filter[is_published]',
      'filter[category_id]',
      'filter[category]',
      'filter[tags]',
      'filter[house]'
    ],
    filter: [],
    isChanged: false,
    sorts: ['id', 'date_published', 'title', 'preview', 'announce_category_id'],
    sort_by: 'title',
    sort_order: 1,
    items: [],
  }),
  getters: {
    list: (store) => store.items,
    paginator: (store) => store.pagination,
    isEmpty: (store) => !store.items.length && store.loaded,
    search: (store) => getFilterByKey(store, 'filter[title]')?.value ?? ""
  },
  actions: {
    async getList(page: number, per_page?: number, sort?: string): Promise<void> {
      // если уже загружали страницу выходим
      if (isExistRecords(this.$state, page, this.loaded, per_page, sort))
        return;
      const elemSort = getSortRecords(this.$state, sort);
      this.loaded = false;
      const { data } = await this.$state.api.getAnnounceList(
          page,
          per_page ?? this.pagination.per_page,
          elemSort,
          getFilterByKey(this.$state, 'filter[title]')?.value as string,
          getFilterByKey(this.$state, 'filter[preview]')?.value as string,
          getFilterByKey(this.$state, 'filter[tags]')?.value as string,
          getFilterByKey(this.$state, 'filter[house]')?.value as number,
          getFilterByKey(this.$state, 'filter[category]')?.value as string,
          getFilterByKey(this.$state, 'filter[category_id]')?.value as number
      );
      this.pagination = data.pagination;
      this.items = data.items;
      this.isChanged = false;
      this.loaded = true;
    },
    init(query?: LocationQuery): void {
      setDefaultCollection(this.$state, query);
    },
    setSearch(value?: string): void {
      setFilter(this.$state, 'filter[title]', value);
    },
    clearFilterByKey(key: string, value?: string): void {
      clearFilter(this.$state, key, value);
    },
    getFilter(key: string): string|number|string[]|boolean {
      return getFilterByKey(this.$state, key)?.value ?? ""
    },
    clearLoaded(): void {
      this.loaded = false;
    }
  }
});
