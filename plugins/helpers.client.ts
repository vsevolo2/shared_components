// @ts-ignore
import Inputmask from 'inputmask';
import * as bootstrap from 'bootstrap';

export default defineNuxtPlugin((nuxtApp) => {
  return{
    provide:{
      helpers: {
        hide_modal: function (modalInstance: Element) {
          bootstrap.Modal.getInstance(modalInstance)?.hide();
        },
        hide_all_modals: function () {
          document.querySelectorAll('.modal').forEach(modal => {
            bootstrap.Modal.getInstance(modal)?.hide()
          })
        },
        number_format(value?: string | number) {
          if (typeof value === undefined)
            return;
          value = parseFloat(value?.toString() || "0") || 0;
          value = value.toFixed(2);
          value = value.replace('.', ',');
          let parts = value.split(',');

          let k = 0;
          let ret = [];
          for (let i = value.length - 1; i >= 0; i--) {
            k++;
            ret.push(parts[0][i]);
            if (k % 3 === 0) {
              ret.push(' ');
            }
          }
          return (ret.reverse().join("")).trim() + ',' + parts[1];
        },

        capitalize(value: string) {
          return value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase();
        }
      }
    }
  }
});
