import globalAxios from 'axios';
// import {useLocal} from "~/composables/useLocal";

type QueueItem = (data: string) => void
export default class ApiTokenInterceptor
{

    public static start(): void
    {
        globalAxios.interceptors.request.use(async function (request) {

            // если идет refresh токена - вмешиваться в http запрос не нужно
            if (String(request.url).indexOf('refresh-token') !== -1)
            {
                return request
            }

            const access_token = await useApiToken().getAccessToken()

            if (access_token)
            {
                const account_id = useLocal().get()
                if (account_id)
                {
                    request.headers['X-Account-Id'] = account_id
                }
                request.headers['Authorization'] = `Bearer ${access_token}`
            }

            return request
        });

    }

}

