export default class Local
{
    private localKey: any;
    private accountId: string | null = null;

    constructor(key: string | number)
    {
        this.localKey = key;
        this.fromStorage()
    }

    public exists(): boolean
    {
        return Boolean(this.accountId)
    }

    public get(): number | undefined
    {
        return this.accountId && this.accountId !== "undefined"
            ? +this.accountId
            : undefined;
    }

    public set(id: number | string | undefined, key = 'accountId'): this
    {
        this.localKey = key
        this.accountId = String(id)
        this.toStorage()
        return this
    }

    public setIfNone(id: number | string | undefined, key = 'accountId'): this
    {
        !this.exists() && this.set(id, key);
        return this
    }

    public clear(): this
    {
        return this.set(undefined)
    }

    public fromStorage()
    {
        if (typeof localStorage != 'undefined')
        {
            this.accountId = localStorage.getItem(this.localKey) ?? null;
        }
        return this
    }

    public toStorage()
    {
        if (typeof localStorage != 'undefined')
        {
            (this.accountId)
                ? localStorage.setItem(this.localKey, this.accountId)
                : localStorage.removeItem(this.localKey)
        }
        return this
    }
}

