import single from '~/composables/singleAsyncProcess';
import { isPwa } from '~/composables/stringHelper';

export default class ApiToken
{
    constructor()
    {
        this.fixProductionAdminToken()
    }

    public set(access_token: string, refresh_token: string): this
    {
        this.store(access_token, refresh_token)
        return this
    }

    public clear(): this
    {
        this.store(null, null)
        return this
    }

    public exists(): boolean
    {
        return Boolean(this.getRefreshToken())
    }

    private getCurrentAccessToken(): string | null
    {
        const access_token = localStorage.getItem('access_token')
        return (access_token && parseInt(localStorage.getItem('access_token_expire') ?? '0') > this.now())
           ?  access_token
           : null
    }

    public expireAccessToken(): void
    {
      return localStorage.removeItem('access_token_expire')
    }

    public async getAccessToken(): Promise<string>
    {
        const currentAccessToken = this.getCurrentAccessToken()
        if (currentAccessToken) {
            return Promise.resolve(currentAccessToken)
        }

        const refreshToken = this.getRefreshToken()
        if (!refreshToken) {
            return Promise.resolve('')
        }

        return new Promise<string>(async (resolve) => {
            const {data} = await single.do<string>("refresh_token", () => this.refreshToken(refreshToken))
            resolve(data)
        });

    }

    public getRefreshToken(): string | null
    {
        return localStorage.getItem('refresh_token')
    }

    /**
     * На продакшене изначально использовался 'adminToken',
     * если мы в localStorage находим этот токен, то перезаписываем его в используемый теперь 'refresh_token'
     */
    private fixProductionAdminToken(): void
    {
        const adminToken = localStorage.getItem('adminToken');
        if (adminToken)
        {
            localStorage.setItem('refresh_token', adminToken)
            localStorage.removeItem('adminToken')
        }
    }

    private store(access_token: string | null, refresh_token: string | null): void
    {
        if (refresh_token && access_token) {
            localStorage.setItem('refresh_token', refresh_token)
            localStorage.setItem('access_token', access_token)
            localStorage.setItem('access_token_expire', String(this.now() + 1680)) // 1680 сек = 28 мин
        } else {
            localStorage.removeItem('refresh_token')
            localStorage.removeItem('access_token')
            localStorage.removeItem('access_token_expire')
        }
    }

    /**
     * Получение unix timestamp для текущего времени
     */
    private now(): number
    {
        return Math.floor(Date.now() / 1000)
    }

    private async refreshToken(refresh_token: string): Promise<string>
    {
        console.log('%c >>>>> refresh token ', 'background: #222; color: #bada55');
        const self = this

        return new Promise<string>((resolve) => {
            const options = {
                'headers': {"Authorization": `Bearer ${refresh_token}`}
            }
            useApi().refreshToken(isPwa() ? 1 : undefined, options)
                .then(({data}) => {
                    // смогли обновить токен
                    self.set(data.access_token, data.refresh_token)
                    resolve(data.access_token)
                })
                .catch((err) => {
                    // 401 - это значит, что refresh token больше не работает
                    // только в это случае очищаем токен
                    // а когда сервер недоступен, или сеть потерялась, то не теряем refresh token
                    if (err.response.status === 401)
                    {
                        self.clear()
                    }
                    resolve('')
                })
        })
    }
}

