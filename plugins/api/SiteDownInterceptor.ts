import globalAxios from 'axios';

export default class SiteDownInterceptor
{
    public static start(): void
    {
        globalAxios.interceptors.response.use(function (response) {
            // Не вмешиваемся в положительные 2xx ответы
            return response;
        }, function (error) {
            // когда апи отвечает статусом 423 - это значит на сайте ведутся какие то работы
            // и в этот момент нужно показывать специальную страницу
            if (error.response?.status === 423) {
                window.location.href = '/423';
            }
            return Promise.reject(error);
        });
    }
}

