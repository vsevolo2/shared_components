import { DefaultApi } from "~/api";
import ApiToken from './ApiToken';
import Local from './Local';
import { DefaultApi as DirectoryApi } from '@/directory/apis/default-api';
import ApiTokenInterceptor from '~/plugins/api/ApiTokenInterceptor';
import SiteDownInterceptor from '~/plugins/api/SiteDownInterceptor';

export default defineNuxtPlugin((ctx) => {

    const groupName = (window ? window.location.hostname : '').split('.').slice(0, -1)[0] ?? 'ors'
    const baseUrl = ctx.$config.public.apiBaseUrl + '/person/' + groupName
    const directoryUrl = ctx.$config.public.apiBaseUrl + '/directory'

    const nuxtApp = useNuxtApp();

    nuxtApp.provide('groupName', groupName);
    nuxtApp.provide('token', new ApiToken());
    nuxtApp.provide('api', new DefaultApi({}, baseUrl));
    nuxtApp.provide('local', new Local('accountId'));
    nuxtApp.provide('directory', new DirectoryApi({}, directoryUrl))

    ApiTokenInterceptor.start()
    SiteDownInterceptor.start()

})
