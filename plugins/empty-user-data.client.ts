import {Profile} from "~/store";

export default defineNuxtPlugin(() => {
  addRouteMiddleware('empty-user-data', async (to, from) => {

    const toast = useToastMessage()
    const user = Profile()
    const local = useLocal()
    const token = useApiToken()

    if(token.exists() && !user.loaded)
      try {
        await user.getProfile()
      } catch(e){
        token.clear()
      }

    if( token.exists() && !user.currentAccount() && user.accountsAreNotEmpty() ){
      const account = user.accounts[0]
      user.setAccount(account)
      local.set(account.id)
    }

    if(token.exists() && user.fio === '' && to.path !== '/app/profile'){
      toast().error('Заполните ФИО')
      return navigateTo('/app/profile')
    }

  },{global:true})
});
