import {Centrifuge} from 'centrifuge';
import {Profile} from '~/store/Profile';
import emitter from '~/composables/emitter';
import AppealCommentMessage from "~/components/centrifugo/AppealCommentMessage.vue";
import { Appeals } from '~/store/Appeals';
import {Chat} from "~/store/Chat";

export default defineNuxtPlugin((ctx) => {

    const centrifugoUrl = ctx.$config.public.centrifugoUrl;

    const toast = useToastMessage();
    const api = useApi();
    const apiToken = useApiToken();
    const user = Profile();
    const appeals = Appeals();
    const chat = Chat();

    const getAppeal = (async (appeal_number: string) => {
        // обновление заявки и чата
        const appeal = await appeals.getAppeal(Number(appeal_number));
        if (appeal) {
            await chat.getList(appeal);
        }
    })

    const c = new Centrifuge(centrifugoUrl, {
        getToken: async () => {
            return await useApiToken().getAccessToken()
        },
    });

    c.on('connected', function (context) {
        console.log('connected', context);
    });

    c.on('subscribed', function (context) {
        console.log('subscribed to server-side channel', context.channel);
    });

    c.on('publication', async function (context) {
        console.log('publication receive from server-side channel', context.channel, context.data);
        switch (context.data.event) {

          case 'person_has_changed_full_access':
              apiToken.expireAccessToken();
              await user.getProfile();
            break

          case 'model_update_event':
                console.log(context.data)
                emitter.$emit('model_update_event', context.data.payload)
                switch (context.data?.payload?.model_name) {
                    case 'App\\Models\\InternalMessageOwner':
                    case 'App\\Models\\InternalMessage':
                        user.profile.unread_messages = context.data.payload.unread_messages;
                        break;
                  case 'App\\Models\\Company':

                    const appealBlocked = context.data.payload.model_data.appeal_blocked;
                    const ipuBlocked = context.data.payload.model_data.input_ipu_blocked;
                    const getAccounts = context.data.payload.model_data.accounts;

                    user.profile.accounts.filter(a => getAccounts.includes(a.account)).map(a => {
                      a.appeal_blocked = appealBlocked;
                      a.input_ipu_blocked = ipuBlocked;
                    })
                    const route = useRoute();
                    // если на текущем аккаунте имеются закртытые(страницы) и мы на одной из них, то выкидываем на главную
                    if ((user.appealBlocked() && route.name.includes('app-appeals'))
                      || (user.inputIpuBlocked() && route.name === 'app-person-meters')) {
                      navigateTo('/');
                    }
                    break;
                }

                break;
            case 'appeal_message_send_event':
                const appeal_number = context.data.data.number_appeal;
                await getAppeal(appeal_number);
                toast().success({
                    component: AppealCommentMessage,
                    props: {
                        id: appeal_number
                    }
                });
                break;
            case 'internal_message_event':

                emitter.$emit('internal_message_event', context.data.payload)

                let internal_message = context.data.payload.internal_message;

                let url = '';

                let _text = (internal_message.title + "\n" + getMessageText(internal_message)).trim();

                url = internal_message?.payload?.link || null;
                if (url)
                  url = '/app' + url
                else {
                  switch (internal_message?.event_key){
                    case 'APPEAL_REJECTED':
                    case 'APPEAL_COMPLETED':
                    case 'APPEAL_TAKE_BY_EMPLOYEE':
                    case 'APPEAL_COMMENT_BY_EMPLOYEE':
                      url = "/app/appeals/" + internal_message.model_data.number + "/view";
                      break;
                    case 'NEW_ANNOUNCE':
                      url = "/app/announces/announce/" + internal_message.payload?.model_data.id;
                      break;
                    case 'APPROACHING_VERIFICATION_PERIOD_IPU':
                    case 'LAST_DAY_INPUT_IPU':
                      url = "/app/person-meters/?account=" + internal_message.payload?.model_data?.account;
                      break;
                    case 'PAY_PERIOD':
                      url = "/app/payments/accounts";
                      break;
                    case 'UPLOAD_EPD':
                      url = "/app/pay-documents";
                      break;
                  }
                }

                if (_text > '') {
                    let opts = {
                        timeout: false,
                    };
                    if (url) {
                        opts.toastClassName = 'linked-toast cursor-pointer';
                        opts.onClick = () => navigateTo(url);
                    }
                    toast().info(_text, opts);
                }


                break;
            default:
                // toast().success(JSON.stringify(context.data));
                break;
        }

    });

    ctx.provide('centrifugo', c);
});
