import * as bootstrap from 'bootstrap'

export default defineNuxtPlugin(nuxtApp => {
    window.bootstrap = bootstrap
})
