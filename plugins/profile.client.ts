import { Profile } from "~/store/Profile";
import { useLocal } from "~/composables/useLocal";

export default defineNuxtPlugin(() => {
  addRouteMiddleware('init', async (from) => {
    const local = useLocal()
    const token = useApiToken()
    const user = Profile();
    if (user.noHasGroup || from.name === '423')
      return ;
    if (!user.groupLoaded)
    {
      try {
        await user.getGroup();
        user.groupLoaded = true;
        user.noHasGroup = false;
      } catch (e) {
        user.noHasGroup = true;
        return navigateTo('/404');
      }
    }
  }, { global: true })
});
