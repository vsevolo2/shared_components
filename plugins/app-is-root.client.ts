import { useApiToken } from "~/composables/useApiToken";

export default defineNuxtPlugin(() => {
  addRouteMiddleware('app-is-root', (to, from) => {
    const token = useApiToken();
    if (to.path == '/')
    {
      return token.exists() ? navigateTo('/app') : navigateTo('/app/auth/login')
    }
  }, { global: true })
});
