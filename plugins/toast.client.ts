import Toast, { POSITION, provideToast } from "vue-toastification";

export default defineNuxtPlugin(nuxtApp => {
   nuxtApp.vueApp.use(Toast, {
    position: POSITION.TOP_RIGHT,
  });
  return {
    provide: {
      provideToast
    }
  }
})
