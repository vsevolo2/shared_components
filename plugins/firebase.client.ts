import {initializeApp} from "firebase/app";
import {getMessaging, isSupported, getToken} from "firebase/messaging";
import {UserDevice} from "~/store/UserDevice";
import { isNotificationSupported } from '~/composables/stringHelper';

export default defineNuxtPlugin(async (ctx) => {
    const windowSupport =  await isNotificationSupported();
    const supported = await isSupported().then(() => {
        return true;
    }).catch(() => {
        console.error()
        return false;
    });

    if ((supported && windowSupport) && ctx.$config.public.firebaseVapidKey) {
        // TODO: Add SDKs for Firebase products that you want to use
        // https://firebase.google.com/docs/web/setup#available-libraries

        // Your web app's Firebase configuration

        const firebaseConfig = {
            apiKey: ctx.$config.public.firebaseApiKey,
            authDomain: ctx.$config.public.firebaseAuthDomain,
            projectId: ctx.$config.public.firebaseProjectId,
            storageBucket: ctx.$config.public.firebaseStorageBucket,
            messagingSenderId: ctx.$config.public.firebaseMessagingSenderId,
            appId: ctx.$config.public.firebaseAppId
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const messaging = getMessaging(app);
        const userDevice = UserDevice();

        const requestPermission = () => {
            // console.log('Requesting permission...');
            Notification.requestPermission().then((permission) => {
                if (permission === 'granted') {
                    // console.log('Notification permission granted.');

                    getToken(messaging, {vapidKey: ctx.$config.public.firebaseVapidKey}).then((currentToken) => {
                        if (currentToken)
                        {
                            // Send the token to your server and update the UI if necessary
                            // ...
                            // console.log('currentToken', currentToken);
                            userDevice.register(currentToken);
                        }
                        else
                        {
                            // Show permission request UI
                            console.log('No registration token available. Request permission to generate one.');
                            // ...
                        }
                    }).catch((err) => {
                        console.log('An error occurred while retrieving token. ', err);
                        // ...
                    });

                }
                else
                {
                    userDevice.unregister();

                }
            });
        };
        // @ts-ignore
        navigator?.permissions.query({name: 'push', userVisibleOnly: true}).then(function (status) {
            // Initial permission status is `status.state`
            status.onchange = () => {
                // Status changed to `this.state`
                if (status.state === 'denied')
                {
                    userDevice.unregister();
                }
            };
        });


        ctx.provide('requestPermission', requestPermission);
    }
});
