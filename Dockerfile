FROM node:18-alpine as build
ENV LANG=C.UTF-8
WORKDIR /app
COPY . ./
RUN npm ci && npm run build

FROM node:18-alpine

COPY --from=build /app/.output /app

ENV LANG=C.UTF-8
WORKDIR /app
EXPOSE 3000

CMD ["/app/server/index.mjs"]
