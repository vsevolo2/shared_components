self.addEventListener('push', event => {
    event.preventDefault();
    const data = event.data.json();
    const backendData = JSON.parse(data.data.payload);

    if ('setAppBadge' in navigator) navigator.setAppBadge(backendData?.unread_messages)

    if (backendData.body)
    {
        event.waitUntil(
            self.registration.showNotification(backendData.title ?? '', {
                body: backendData.body,
                icon: backendData.icon ?? null,
                image: backendData.image ?? null,
                badge: backendData.badge ?? null,
                tag: backendData.tag ?? null,
                data: backendData
            })
        );

        event.waitUntil(
            clients
                .matchAll({
                    type: "all",
                    includeUncontrolled: true,
                })
                .then((clientList) => {
                    for (const client of clientList)
                    {
                        // && "focus" in client
                        if (client.visibilityState === 'visible')
                        {
                            return true;
                        }
                    }
                })
        );

    }
});

self.addEventListener("notificationclick", (event) => {
  event.notification.close();

  console.log('eventevent', event.notification.data.event_key)

  let url = "/app/internal-messages";
  switch (event.notification?.data?.event_key ?? '') {
    case "APPEAL_COMPLETED":
    case "APPEAL_TAKE_BY_EMPLOYEE":
    case "APPEAL_COMMENT_BY_EMPLOYEE":
    case "APPEAL_REJECTED":
      url = "/app/appeals/" + event.notification?.data?.payload.model_data.number + "/view";
      break;
    case "LAST_DAY_INPUT_IPU":
    case "APPROACHING_VERIFICATION_PERIOD_IPU":
      url = "/app/person-meters/";
      break;
    case "PAY_PERIOD":
      url = "/app/payments/accounts/";
      break;
    case "UPLOAD_EPD":
      url = "/app/pay-documents/";
      break;
    case "NEW_ANNOUNCE":
        url = "/app/announces/announce/" + event.notification?.data?.payload.model_data.id;
        break;
  }

  // This looks to see if the current is already open and
  // focuses if it is
  event.waitUntil(
    clients
      .matchAll({
        type: "all",
        includeUncontrolled: true,
      })
      .then((clientList) => {
        for (const client of clientList) {
          if (client.url === url && "focus" in client) {
            return client.focus();
          }
        }
        if (clients.openWindow) {
          return clients.openWindow(url);
        }
      })
  );
});
