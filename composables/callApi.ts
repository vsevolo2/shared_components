import useApiErrors from '~/composables/useApiErrors';
import cleanErrors from '~/composables/cleanErrors';

export default async function callApi(closure: any, isLoading?: any, errors?: any): Promise<any> {

    try
    {
        errors && cleanErrors(errors);
        isLoading && (isLoading.value = true);
        return await closure();
    }
    catch (e)
    {
        const {apiErrors} = useApiErrors(e);
        errors !== undefined && (errors = Object.assign(errors, apiErrors));
        console.log(errors,'errors')

    }
    finally
    {
        isLoading && (isLoading.value = false);
    }
}
