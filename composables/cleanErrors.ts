export default async function cleanErrors(data: any) {
    data && Object.keys(data).forEach((k) => {
        delete data[k];
    });
}
