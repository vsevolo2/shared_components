
export type SingleResponse<T> = {
    data: T,
    error: any
}

type QueueItem<T> = (value: SingleResponse<T>) => void
type QueueCollection = {
    [key: string]: QueueItem<any>[];
}

class SingleAsyncProcess
{
    private queues : QueueCollection = {}

    public async do<T>(queue_name: string, cb: () => Promise<T>): Promise<SingleResponse<T>>
    {
        const self = this

        return new Promise<SingleResponse<T>>((resolve) => {

            let started: boolean = true;
            if (!self.queues.hasOwnProperty(queue_name))
            {
                started = false
                self.queues[queue_name] = []
            }

            self.queues[queue_name].push(resolve)

            if (!started) {

                cb()
                    .then((data: T) => {
                        self.resolve_queue(queue_name, {data, error: null})
                    })
                    .catch((error) => {
                        self.resolve_queue(queue_name, {data: null, error})
                    })
            }
        })
    }

    private resolve_queue<T>(queue_name: string, data: SingleResponse<T>): void {
        if (this.queues.hasOwnProperty(queue_name))
        {
            this.queues[queue_name].forEach(cb => cb(data))
            delete this.queues[queue_name]
        }
    }
}


export const SingleProcess = new SingleAsyncProcess()
export default SingleProcess
