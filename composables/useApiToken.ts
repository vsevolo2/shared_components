import ApiToken from '@/plugins/api/ApiToken';

export const useApiToken = (): ApiToken => {
  const nuxtApp = useNuxtApp()
  return nuxtApp.$token
}
