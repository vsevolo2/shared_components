import Local from '@/plugins/api/Local';

export const useLocal = (): Local => {
  const nuxtApp = useNuxtApp()
  return nuxtApp.$local
}
