import { Paginator } from "~/api";
import { Paginator as PaginatorDirectory } from "~/directory";
import { LocationQuery } from 'vue-router';
import { isArray } from '@vue/shared';
import { setFilterParams } from "~/composables/stringHelper";

export interface sortInterface {
    filters?: string[],
    filter?: { key: string; value?: string|string[] }[]
    sorts: string[],
    sort_by: string,
    sort_order: number
}

export interface collectionInterface {
    pagination: Paginator | PaginatorDirectory,
    filter_default?: Record<string, string>, // упрощенные фильтры для справочников
    filters?: string[],
    filter?: { key: string; value?: string|string[] }[],
    isChanged?: boolean,
    loaded?: boolean,
    sorts: string[],
    sort_by: string,
    sort_order: number
}

export default function useCollectionStore() {
    const MAX_PER_PAGE = 100;

    const pagination =  {
        current_page: 1,
        from: 1,
        to: 0,
        last_page: 0,
        per_page: 15,
        total: 0
    }

    const isExistRecords = (
        elem: collectionInterface,
        page?: number,
        loaded?: boolean,
        per_page?: number,
        sort?: string | undefined
    ): boolean => {
        return (
            !!loaded &&
            page === elem.pagination.current_page &&
            (per_page === elem.pagination.per_page || per_page === undefined) &&
            (!sort  ||
                (elem.sort_by === sort) && (
                    isDescSort(elem.sort_by) && elem.sort_order < 0 || !isDescSort(elem.sort_by) && elem.sort_order > 0
                )
            ) &&
            !elem.isChanged
        );
    }

    const setPerPagePagination = (pagination: Paginator, per_page?: number): void => {
        if (per_page && per_page !== pagination.per_page)
            pagination.per_page = per_page;
    }

    const setDefaultCollection = (
        elem: collectionInterface,
        locationQuery?: LocationQuery
    ): void => {
        const route = useRoute();
        const query = locationQuery ?? route.query;
        if (locationQuery) {
            return;
        }

        if (query.page)
            elem.pagination.current_page = parseInt(query.page as string);
        if (query.per_page)
            elem.pagination.per_page = parseInt(query.per_page as string);

        if (Number(elem.pagination?.per_page) > MAX_PER_PAGE)
            elem.pagination.per_page = MAX_PER_PAGE;

        if (query.sorting && elem.sorts.includes(getAbsSort(query.sorting as string))) {
            elem.sort_by = query.sorting as string;
        }

        elem.filters = setFilterParams(elem, query)
    }

    const getSortRecords = (list: collectionInterface, sort?: string): string | undefined => {
        if (!sort) return undefined;
        if (sort === getAbsSort(list.sort_by)) {
            list.sort_order = isDescSort(list.sort_by) ? -1 : 1;
            return list.sort_by;
        }
        list.sort_order = isDescSort(sort) ? -1 : 1;
        return sort;
    }

    // получение значения фильтра по ключу
    const getFilterByKey = (
        list: { filter: { key: string; value?: string|string[]|number }[] },
        key: string
    ): { key: string; value?: string|number|string[]|number } | undefined => {
        return list.filter.find(f => f.key === key) ?? undefined
    }

    // передача фильтра по ключу
    const setFilter = (list: {
        filter: { key: string; value?: string|string[]|number }[],
        isChanged?: boolean
    }, key: string, value?: string|string[]|number): void => {
        const search = getFilterByKey(list, key);
        if (search)
            search.value = value ?? undefined;
        else
            list.filter.push({key, 'value': value ?? ""});
        list.isChanged = true;
    }

    // чистка фильтра по ключу или всех фильтров
    const clearFilter = (
        list: {
            filter: { key: string; value?: string|string[] }[],
            isChanged?: boolean
        },
        key: string,
        value?: string
    ): void => {
        const search = getFilterByKey(list, key);
        if (!search) return;
        if (isArray(search.value)) {
            search.value = value ? (search.value as string[]).filter(v => v!==value) : [];
        }
        else
            search.value = undefined;
        list.isChanged = true;
    }

    return {
        pagination,
        isExistRecords,
        setPerPagePagination,
        setDefaultCollection,
        getSortRecords,
        getFilterByKey,
        setFilter,
        clearFilter
    };
}

export const isDescSort = (value?: string): boolean => {
    if (!value) return false;
    return value.slice(0, 1) === '-';
}

export const getAbsSort = (value?: string): string => {
    if (!value) return '';
    return isDescSort(value) ? value.slice(1) : value;
}

export const getSortClass = (list: collectionInterface, column: string): string => {
    if (getAbsSort(list.sort_by) !== column || !list.sort_order) return '';
    return isDescSort(list.sort_by) ? 'desc': 'asc';
}

export const getRequestSort = (list: collectionInterface, column: string): string => {
    if (getAbsSort(list.sort_by) !== column || !list.sort_order) return column;
    return list.sort_by;
}

export const changeSortRecords = (list: collectionInterface, sort?: string): void => {
    if (!sort) return undefined;
    if (sort === getAbsSort(list.sort_by)) {
        if (!isDescSort(list.sort_by)) {
            list.sort_by = '-' + sort;
            return;
        }
    }
    list.sort_by = sort;
}

export const getCollectionPage = (
    elem: collectionInterface,
    numberPage: number,
    perPage?: number,
    column?: string
): void => {
    const route = useRoute();
    const router = useRouter();
    let query = {
        page: numberPage,
        per_page: perPage ?? elem.pagination.per_page,
        sorting: getRequestSort(elem, elem.sort_by),
    }
    if (column && elem.sorts.includes(column)) {
        query.sorting = getRequestSort(elem, column);
    }

    if (elem.filter) {
        elem.filter.map(f => {
            if (!f.value || !f.key) return;
            query = {...query, ...{[f.key]: f.value}}
        })
    }

    router.push({
        path:route.path,
        query: query
    });
}
