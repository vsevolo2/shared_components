import { useToastMessage } from "~/composables/useToastMessage";

export default function useApiErrors(axiosErrors: any, onlyShowErrors = false, hasMessage = true):
    { message: any; apiErrors: {}; }
{
  let apiErrors = reactive({});
  if (axiosErrors?.response?.status === 422) {
    apiErrors = axiosErrors.response.data.errors;
  }
  const message = axiosErrors.response.data.message;
  const toast = useToastMessage();

  if (onlyShowErrors) {
    if (!axiosErrors.response.data.errors) {
      toast().error(axiosErrors.response.data.message);
      // @ts-ignore
      return ;
    }
    Object.keys(axiosErrors.response.data.errors).map(key => {
      toast().error(axiosErrors.response.data.errors[key][0]);
    });
    return  { message, apiErrors };
  }

  // hasMessage: показ сообщения по умолчанию
  if (message && hasMessage) {
    toast().error(message);
  }

  return { message, apiErrors }
}
