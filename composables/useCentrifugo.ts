import { Centrifuge } from 'centrifuge'

export const useCentrifugo = () : Centrifuge => {
  const nuxtApp = useNuxtApp()
  return nuxtApp.$centrifugo
}
