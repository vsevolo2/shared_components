// константы статусов
import { Appeal } from "~/api";

export const APPEAL_NEW = 0;
export const APPEAL_IN_WORK = 1;
export const APPEAL_CANCELED = 2;
export const APPEAL_COMPLETE = 3;
export const APPEAL_CLOSED = 4;

export const appealSteps = [
    {
        type: "object",
        text: "Объект заявки",
    },
    {
        type: "type",
        text: "Тип заявки"
    },
    {
        type: "category",
        text: "Категория заявки",
    },
    {
        type: "job",
        text:  "Тематика заявки",
    },
    {
        type: "default",
        text:  "Форма заявки"
    }
];

export const modelTypes = {
    EPD: "App\\Models\\EPD",
    IPU: "App\\Models\\PersonMeter",
    announce: "App\\Models\\Announce",
    appeal: "App\\Models\\Appeal"
}

export const useAuditableTypes = (): Record<string, string> => {
    return {
        '-1': 'Любой',
        'App\\Models\\Announce': 'Объявление',
        'App\\Models\\Appeal': 'Заявка',
        'App\\Models\\AppealCategory': 'Категория заявки',
        'App\\Models\\AppealType': 'Тип заявки',
        'App\\Models\\AppealJob': 'Вид работ',
        'App\\Models\\User\\Employee': 'Сотрудник',
        'App\\Models\\User\\EmployeeAccount': 'Аккаунт сотрудника',
        'App\\Models\\PersonPersonalAccount': 'Связки жилец-счет',
        'App\\Models\\PersonalAccount': 'ЛС',
        'App\\Models\\Task': 'Задача',
        'App\\Models\\HouseMeter': 'ОПУ',
        'App\\Models\\FlatMeter': 'КПУ',
        'App\\Models\\PersonMeter': 'ИПУ',
        'App\\Models\\HouseGroup': 'Участок',
        'App\\Models\\MessageTemplate': 'Шаблон уведомления',
    };
};

export const useAppealStatus = () : Record<string, string> => {
    return {
        0: 'Новая',
        1: 'В работе',
        2: 'Отменена',
        3: 'Выполнена',
        4: 'Закрыта'
    }
}

export const appealStatusClass = (value?: number): string => {
    switch (value) {
        case 1: return "bg-purple";
        case 2: return "bg-danger";
        case 3: return "bg-success";
        case 4: return "bg-cyan";
        default: return "bg-primary";
    }
}

export const isAppealComplete = ((status: number) => {
    return status === APPEAL_COMPLETE;
});

export const showEstimateBtn = ((appeal: Appeal) => {
    return !appeal.rating && appeal.status === APPEAL_COMPLETE;
});

export const isAppealNew = ((status: number) => {
    return status === APPEAL_NEW;
});

export const brandingOptions = {
    favicon: '',
    logo: '',
    backgroundColor: '#f1f5f9',
    pageTitleColor: '#616876',
    footerBgColor: '#ffffff',
    footerColor: '#616876',
    headerLogoTitle: 'ОРС',
    headerBgColor: '#ffffff',
    headerColor: '#616876',
    headerBottomBgColor: '#ffffff',
    headerBottomColor: '#616876',
    btnSuccessBgColor: '#206bc4',
    btnSuccessColor: '#ffffff'
};
