import * as pkg from 'vue-toastification';

export const useToastMessage = () => {
  const { useToast } = pkg;
  return useToast;
}
