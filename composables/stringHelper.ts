import {collectionInterface} from "~/composables/useCollectionStore";
import { LocationQuery } from "vue-router";
import { InternalMessage } from '~/api'

export async function isNotificationSupported(): Promise<boolean> {
    // firebase-js-sdk/issues/2393 reveals that idb#open in Safari iframe and Firefox private browsing
    // might be prohibited to run. In these contexts, an error would be thrown during the messaging
    // instantiating phase, informing the developers to import/call isSupported for special handling.
    return (
        typeof window !== 'undefined' && navigator &&
        'serviceWorker' in navigator &&
        'PushManager' in window &&
        'Notification' in window &&
        'fetch' in window &&
        ServiceWorkerRegistration.prototype.hasOwnProperty('showNotification') &&
        PushSubscription.prototype.hasOwnProperty('getKey')
    );
}

export const formatPhone = (phone?: string) : string => {
    if (!phone) return "";
    const parts = phone.replace(/\D/g, '').match(/(\d)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
    if (!parts) return phone;
    return "+" + parts[1] + " (" + parts[2] + ") " + [parts[3], parts[4], parts[5]].join("-");
}

export const addressShort = (addressData: string): Record<string, string> => {
    let flat = "";
    let address = "";
    const addressMass = addressData.split(',');
    if (addressMass.length) {
        address = addressMass[1].trim();
        flat = addressMass[addressMass.length - 1].replace(' КВ. ', "").trim();
    }

    return { address, flat }
}

export const meterDeviceStatusClass = (value?: string): string => {
    switch (value) {
        case 'НЕАКТИВНЫЙ': return "bg-danger";
        case 'АКТИВНЫЙ': return "bg-success";
        default: return "bg-primary";
    }
}

export const getMonthName = (date = new Date) => [
    'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
    'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',
][date.getMonth()];

export const formatDateToString = (value: string, order = { year: 2, month: 1, day: 0 }, separator = '.'): string => {
    const dateArray = value.split(separator);
    return [
        dateArray[order.year],
        dateArray[order.month],
        dateArray[order.day]
    ].join('-')
}

// форматирование даты
export const formatDate = (value?: string, options = {
    month: '2-digit',
    day: '2-digit',
    year: 'numeric'
} as Intl.DateTimeFormatOptions | undefined): string => {
    if (!value) return "";
    const date = new Date(value);
    if (!options) return date.getFullYear() + '-' + date.getUTCMonth() + '-' + date.getDay();
    return date.toLocaleString(['ru-RU'], options);
}

// сравнение времени
export const isDateInPast = (date: string, time: string | undefined): boolean => {
    const now = new Date()
    const range = date.split('.');
    const otherDate = new Date(Number(range[2]), Number(range[1]) - 1, Number(range[0]));
    let Minutes = 1;
    if (time) {
        const range = time.split(':');
        const hours = Number(range[0]);
        const min = Number(range[1]);
        Minutes =  hours * 60 + min
        otherDate.setMinutes(Minutes)
    }
    return now.valueOf() > otherDate.valueOf();
};

// дата со временем
export const formatDateTime = (value: string, options?: Intl.DateTimeFormatOptions): string => {
    return formatDate(value, options ?? {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    })
}

export const blobToImage = (blob: Blob): HTMLImageElement => {
    const url = URL.createObjectURL(blob)
    let img = new Image()
    img.onload = () => {
        URL.revokeObjectURL(url)
    }
    img.src = url
    return img;
}

export const stringFloatWithFixed = (strNum: string, precession: number): string => {
    return parseFloat(strNum).toFixed(precession);
}

export const parseFullMask = (strNum: string, digits: number, precession: number): string => {
    if (!strNum)
        return '';
    let strPlaceholder = '';
    const parseNumber = parseInt(strNum);
    // число символов до запятой
    const num = digits - String(parseNumber).length;
    // добавляем нули до запятой
    for (let i = 0; i < num; i++)
        strPlaceholder += "0";
    return strPlaceholder + stringFloatWithFixed(strNum, precession);
}

export const getServiceIcon = ((item: any, isService = false) => {
    if (!item)
        return 'background-image: url(/icons/neutral_counter.svg); background-size: contain;';
    const name = isService ? item.short_name : item.service?.short_name;
    if (name.includes('ХОЛ.ВОДА')) {
        return 'background-image: url(/icons/cold_water.svg); background-size: contain;';
    }
    if (name.includes('ГОР.ВОДА')) {
        return 'background-image: url(/icons/hot_water.svg); background-size: contain;';
    }
    return 'background-image: url(/icons/neutral_counter.svg); background-size: contain;';
});

export const setFilterParams = (elem: collectionInterface, locationQuery: LocationQuery): string[] | undefined => {
    if (elem.filters && (elem.isChanged || !elem.loaded)) {
        elem.filter = [];
        elem.filters.map(f => {
            if (!locationQuery[f]) return;
            elem.filter?.push({
                key: f,
                value: locationQuery[f] as string
            })
        });
    }
    return elem.filters;
}

export const parseMask = (precision: number, digits: number, symbol = '0'): string => {
    let strPlaceholder = "";
    // символы до запятой
    for (let i = 0; i < digits; i++)
        strPlaceholder += symbol;
    // после запятой
    for (let i = 0; i < precision; i++) {
        if (!i)
            strPlaceholder += ". ";
        strPlaceholder += symbol;
    }
    return strPlaceholder;
}

export interface FileSize {
    bytes: number,
    size: string,
    unit: string,
    human: string
}

export const getHumanFileSize = (originalBytes?: string): FileSize => {
    const units = ['B','KB', 'MB','GB']

    const res = {
        bytes: 0,
        size: '0',
        unit: units[0],
        human: '0 ' + units[0],
    } as FileSize

    const bytes = parseInt(originalBytes ?? '0')
    res.bytes = (isNaN(bytes) || bytes <= 0) ? 0 : bytes;

    let size = res.bytes

    for (let i = 0; i < units.length; i++) {
        res.size = String(Number(size.toFixed(2)));
        res.unit = units[i];

        if (size < 1000) {
            break;
        } else if (size <= 1024) {
            res.size = "1";
            res.unit = units[i + 1] ?? 'TB';
            break;
        }
        size /= 1024
    }

    res.human = res.size + ' ' + res.unit

    return res
}

export const parseError = (name: keyof typeof errors, errors: Record<string, string>): string => {
    if (!name) {
        return "";
    }
    if (!errors) {
        return "";
    }
    const fieldError = errors[name];
    if (!fieldError) {
        return "";
    }
    if (Array.isArray(errors[name])) {
        return fieldError[1] ?? fieldError[0];
    }
    return fieldError;
}

export const getMessageText = (internal_message: {body: string, payload: { model_data: any }}): string => {
    if (!internal_message.payload || !internal_message.payload.model_data)
        return internal_message.body;
    let text = internal_message.body;
    Object.keys(internal_message.payload.model_data).forEach((key: string) => {
        text = text.replaceAll("{model_data." + key +"}", internal_message.payload.model_data[key]);
    });
    return text;
}

export const isPwa = (): boolean =>  {
  return window.matchMedia('(display-mode: standalone)').matches;
}

export const parseDate = (dateString: string) => {
  const [date, time] = dateString.split(' ');
  const [day, month, year] = date.split('.');
  const [hours, minutes] = time.split(':');
  return new Date(`${year}-${month}-${day}T${hours}:${minutes}`);
};

// Функция, которая проверяет, создано ли сообщение сегодня
export const isToday = (createdAt: string, today: Date) => {
  const [day, monthNumber, year] = createdAt.slice(0, 10).split('.').map(Number)
  const messageDate = new Date(year, monthNumber - 1, day);
  return (
    messageDate.getDate() === today.getDate() &&
    messageDate.getMonth() === (today.getMonth()) &&
    messageDate.getFullYear() === today.getFullYear()
  );
};

// Функция, которая проверяет, создано ли сообщение вчера
export const isYesterday = (createdAt: string, yesterday: Date) => {
  const [day, monthNumber, year] = createdAt.slice(0, 10).split('.').map(Number)
  const messageDate = new Date(year, monthNumber - 1, day);
  return (
    messageDate.getDate() === yesterday.getDate() &&
    messageDate.getMonth() === (yesterday.getMonth()) &&
    messageDate.getFullYear() === yesterday.getFullYear()
  );
};

export const renderTimeAgoText = (messageDate: Date, today: Date): string  => {
  const minutesAgo = Math.floor((today.getTime() - messageDate.getTime()) / 60000);
  const lastDigit = minutesAgo % 10
  const text = ['только что', 'минуту назад', 'минуты назад', 'минут назад']
  if (minutesAgo < 1) return text[0];
  if (minutesAgo === 1 || minutesAgo > 20 && lastDigit === 1) return `${minutesAgo} ${text[1]}`;
  if (minutesAgo > 1 && minutesAgo < 5
    || minutesAgo > 20 && lastDigit > 1 && lastDigit < 5
  ) return `${minutesAgo} ${text[2]}`;
  return `${minutesAgo} ${text[3]}`;
}

export const isWithinLastHour = (messageDate: Date, today: Date) => {
  const timeDifference = today.getTime() - messageDate.getTime();
  return Math.floor(timeDifference / (1000 * 60)) < 60;
};

export const isItemExpandable =  (event: MouseEvent) => {
  const element = event.currentTarget as HTMLElement;
  const title = element.querySelector('#title') as HTMLElement
  const body = element.querySelector('#body') as HTMLElement

  return title?.scrollWidth > title?.offsetWidth || body?.scrollWidth > body?.offsetWidth;
}

export const toggleExpand =  (event: MouseEvent) => {
  const element = event.currentTarget as HTMLElement;
  element?.classList?.toggle("expand")
}

export const handleNotificationClick = async (
  event: MouseEvent, item: InternalMessage, read: Function, location: string
) => {
  const notificationsMenu = document.getElementById('notifications') as HTMLElement;
  if (isItemExpandable(event))
  {
    toggleExpand(event)
    if (location === 'table' && !item.read_tmstmp) await read(item)
    return
  }
  if (!item.read_tmstmp) await read(item)
  if (item.internal_message?.payload?.link)
  {
    await useRouter().push(`/app${item.internal_message?.payload?.link}`)
    if (location === 'header') notificationsMenu && notificationsMenu.classList.remove('show')
  }
  if (location === 'table') toggleExpand(event)
}

export const maskedCardFormat = (originCard:string): string => {
  return originCard.substring(8).replaceAll('x','*')
}
