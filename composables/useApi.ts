import { DefaultApi } from '@/api/apis/default-api';
import { DefaultApi as DirectoryApi } from '@/directory/apis/default-api';

export const useApi = () : DefaultApi => {
  const nuxtApp = useNuxtApp()
  return nuxtApp.$api
}

export const useDirectoryApi = () : DirectoryApi => {
  const nuxtApp = useNuxtApp()
  return nuxtApp.$directory
}
