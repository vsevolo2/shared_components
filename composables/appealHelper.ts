import {reactive} from "vue";
import {Appeal, AppealJob, AppealPlace, AppealRequest, AppealType, FileResource} from "~/api";
import {APPEAL_CANCELED} from "~/composables/useConst";

export interface AppealForm {
    id: string,
    desc: string,
    number: string,
    phone: string,
    visit_at?: string,
    visit_time_range?: string,
    closed_at?: string,
    floor?: number,
    entrance?: number,
    status?: number,
    pay_status?: string,
    pay_status_label?: string,
    rating?: '',
    files: Array<FileResource|Blob>
}

export const steps = reactive({
    object: {} as AppealPlace,
    type: {} as AppealType,
    category: {} as any,
    job: {} as AppealJob,
    form: {} as AppealForm
});

export const chooseObject = (item: AppealPlace, step: keyof typeof steps): void => {
    steps[step] = item;
}

export const stepsClear = (): void => {
    // @ts-ignore
    Object.keys(steps).map((step: keyof typeof steps) => {
        steps[step] = {};
    })
}

export const getStepByName = (step: string): keyof typeof steps => {
    // @ts-ignore
    return steps[step]
}

// данные для Отмены заявки
export const getAppealRequest = (accountID: number, selectedAppeal: Appeal, reason: string): AppealRequest | undefined => {
    if (!accountID || !selectedAppeal.id) return;
    const request = {} as AppealRequest;
    // общие данные Заявки;
    // Object.assign(request, selectedAppeal);
    // данные для Отмены
    Object.assign(request, {
        personal_account_id: accountID,
        appeal_place_id: selectedAppeal.place.id,
        reason: reason,
        status: APPEAL_CANCELED,
        command: 'reject'
    })
    return request as AppealRequest;
}
