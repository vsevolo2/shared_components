import { computed }  from "vue";
// import { ValidationErrors } from "~/api";

/**
 * Миксин для полей ввода формы
 *
 * @param props
 * @param emit
 */
export default function useFieldForm(
  props: {
    name?: string;
    type?: string;
    modelValue?: string;
    errors?: {[key: string]: any | string}
  },
  emit: any
) {

  const model = computed({
    get () {
      return props.modelValue;
    },
    set (value) {
      return emit('update:modelValue', value)
    }
  });
  const name = props?.name as string;
  const hasError = name ? computed((): boolean => !!props?.errors && !!props?.errors[name]) : false;
  const error = computed((): string => {
    if (!name) return "";
    const errors = props?.errors;
    if (!errors) return "";
    const fieldError = errors[name];
    if (!fieldError) return "";
    if (Array.isArray(errors[name]))
      return fieldError[0];
    return fieldError as string;
  });
  return { model, hasError, error, name }
}
