import {HouseFlat} from '~/api';

const flatNumberParts = (a: string): [any, any] => {

    // обычное представление - сначала идут цифры - затем другие знаки (или пусто)
    // пример 1, 1А, 2, 2А, 2Б...
    const re = /^(\d+)(.*)$/;
    const m = a.match(re);

    // добиваемся логики - сначала идут нормальные номера, начинающиеся с цифр, а затем все остальное
    // для этого, в случае если "не нормальный" номер квартиры,
    // то первый элемент в ответе - любой символ больше цифры 9... например 'a',

    return m ? [parseInt(m[1]), m[2]] : ['a', a]
}
export const HouseFlatSorterFunc = (a: HouseFlat, b: HouseFlat) => {
    // сортировка по номеру квартиры
    // 1, 1А, 2, 2А, 2Б...

    const ap = flatNumberParts(a.number)
    const bp = flatNumberParts(b.number)

    if (ap[0] < bp[0]) {
        return -1
    }
    if (ap[0] > bp[0]) {
        return 1
    }

    if (ap[1] < bp[1]) {
        return -1
    }
    if (ap[1] > bp[1]) {
        return 1
    }

    return 0
}
