const hmrPort = (process.env.HMR_PORT || 29115) as number

export default defineNuxtConfig({
    ssr: false,
    app: {
        head: {
            meta: [
                { charset: 'utf-8' },
                { name: 'apple-mobile-web-app-capable', content: 'yes' },
                { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
                { name: 'format-detection', content: 'telephone=no' },
                { name: 'HandheldFriendly', content: 'True' },
            ],
            link: [
                {  rel: 'manifest', href: '/manifest.json' },
            ]
        },
        pageTransition: { name: 'page', mode: 'out-in' }
    },
    plugins:[
      '~/plugins/api/index.ts',
    ],
    css: [
        // tabler.io
        'vue-toastification/dist/index.css',
        '@tabler/core/dist/css/tabler.min.css',
        '@tabler/core/dist/css/tabler-vendors.min.css',
        '@tabler/core/dist/libs/dropzone/dist/dropzone.css',
        '@vuepic/vue-datepicker/dist/main.css',
        // our css
        '@/assets/sass/index.scss',
        'ant-design-vue/dist/antd.css'
    ],
    typescript: {
        strict: true
    },
    modules: [
        '@nuxtjs/device',
        '@vite-pwa/nuxt',
        '@pinia/nuxt'
    ],
    vite: {
        server: {
            hmr: {
                port: hmrPort,
                clientPort: hmrPort
            }
        }
    },
    runtimeConfig: {
        // Private keys are only available on the server
        // Public keys that are exposed to the client
        public: {
            // Can be overridden using environment variable 'NUXT_PUBLIC_API_BASE_URL'
            apiBaseUrl: (process.env.NUXT_PUBLIC_API_BASE_URL || 'https://api.gkh911.ru/api'),

            firebaseVapidKey: '',
            firebaseApiKey: '',
            firebaseAuthDomain: '',
            firebaseProjectId: '',
            firebaseStorageBucket: '',
            firebaseMessagingSenderId: '',
            firebaseAppId: '',

// Can be overridden using environment variable 'NUXT_PUBLIC_GROUP_POSTFIX'
            groupPostfix: '',
            // Can be overridden using environment variable 'NUXT_PUBLIC_CENTRIFUGO_URL'
            centrifugoUrl: 'wss://api.gkh911.ru/webcentrifugo/connection/websocket',
        }
    },
});
